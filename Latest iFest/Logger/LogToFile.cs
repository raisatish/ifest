﻿using Interfaces;
using Models;
using System;
using System.IO;
using System.Text;

namespace Logger
{
    public class LogToFile : ILog
    {
        private readonly string LogFilePath;        

        public LogToFile()
        {
            LogFilePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "iFest");
            if (!Directory.Exists(LogFilePath))
                Directory.CreateDirectory(LogFilePath);

            LogFilePath = Path.Combine(LogFilePath, "Log_" + DateTime.Today.ToString("yyyyMMdd") + ".log");
        }

        public void Log(string Message)
        {
            using (var lf = new FileStream(LogFilePath, FileMode.Append, FileAccess.Write))
            {
                //byte[] b = Encoding.ASCII.GetBytes($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} - {"Information".PadRight(12, ' ')} - {Message}\r\n");
                byte[] b = Encoding.ASCII.GetBytes(String.Format("{0} - {1} - {2}\r\n", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "Information".PadRight(12, ' '), Message)); 
                lf.Write(b, 0, b.Length);
            }
        }

        public void Log(string Message, LogType LT)
        {
            using (var lf = new FileStream(LogFilePath, FileMode.Append, FileAccess.Write))
            {
                //byte[] b = Encoding.ASCII.GetBytes($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} - {LT.ToString().PadRight(12, ' ')} - {Message}\r\n");
                byte[] b = Encoding.ASCII.GetBytes(String.Format("{0} - {1} - {2}\r\n", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "Information".PadRight(12, ' '), Message)); 
                lf.Write(b, 0, b.Length);
            }
        }

        public void Log(Exception ex)
        {
            using (var lf = new FileStream(LogFilePath, FileMode.Append, FileAccess.Write))
            {
                //byte[] b = Encoding.ASCII.GetBytes($"{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")} - {"Information".PadRight(12, ' ')} - {ex.ToString()}\r\n");
                byte[] b = Encoding.ASCII.GetBytes(String.Format("{0} - {1} - {2}\r\n", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "Information".PadRight(12, ' '), ex.ToString())); 
                lf.Write(b, 0, b.Length);
            }
        }
    }
}