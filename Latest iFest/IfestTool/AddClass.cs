﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IfestTool
{
    public partial class frmAddClass : Form
    {
        public frmAddClass()
        {
            InitializeComponent();
        }

        public string ClassName
        {
            get;
            private set;
        }

        private void frmAddClass_Load(object sender, EventArgs e)
        {
            ClearData();
        }

        private void btnAddClass_Click(object sender, EventArgs e)
        {
            if (!IsValidClass())
            {
                MessageBox.Show("Please provide Class Name.", "Error !");
                this.DialogResult = DialogResult.None;
            }

            this.ClassName = txtClassName.Text.Trim();
        }

        private bool IsValidClass()
        {
            bool isValid = false;

            isValid = txtClassName.Text.Trim().Length > 0;

            return isValid;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            ClearData();

            this.Close();
        }

        private void ClearData()
        {
            txtClassName.Text = string.Empty;
            this.ClassName = string.Empty;
        }

        private void frmAddClass_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.None)
            {
                e.Cancel = true;
            }
        }
    }
}
