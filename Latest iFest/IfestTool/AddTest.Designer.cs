﻿namespace IfestTool
{
    partial class frmAddTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTestName = new System.Windows.Forms.Label();
            this.txtTestName = new System.Windows.Forms.TextBox();
            this.btnCancelTest = new System.Windows.Forms.Button();
            this.btnSaveTest = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblTestName
            // 
            this.lblTestName.AutoSize = true;
            this.lblTestName.Location = new System.Drawing.Point(20, 26);
            this.lblTestName.Name = "lblTestName";
            this.lblTestName.Size = new System.Drawing.Size(59, 13);
            this.lblTestName.TabIndex = 0;
            this.lblTestName.Text = "Test Name";
            // 
            // txtTestName
            // 
            this.txtTestName.Location = new System.Drawing.Point(96, 23);
            this.txtTestName.Name = "txtTestName";
            this.txtTestName.Size = new System.Drawing.Size(210, 20);
            this.txtTestName.TabIndex = 2;
            // 
            // btnCancelTest
            // 
            this.btnCancelTest.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancelTest.Location = new System.Drawing.Point(58, 73);
            this.btnCancelTest.Name = "btnCancelTest";
            this.btnCancelTest.Size = new System.Drawing.Size(75, 23);
            this.btnCancelTest.TabIndex = 4;
            this.btnCancelTest.Text = "Cancel";
            this.btnCancelTest.UseVisualStyleBackColor = true;
            this.btnCancelTest.Click += new System.EventHandler(this.btnCancelTest_Click);
            // 
            // btnSaveTest
            // 
            this.btnSaveTest.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnSaveTest.Location = new System.Drawing.Point(168, 73);
            this.btnSaveTest.Name = "btnSaveTest";
            this.btnSaveTest.Size = new System.Drawing.Size(111, 23);
            this.btnSaveTest.TabIndex = 5;
            this.btnSaveTest.Text = "Add Test";
            this.btnSaveTest.UseVisualStyleBackColor = true;
            this.btnSaveTest.Click += new System.EventHandler(this.btnSaveTest_Click);
            // 
            // frmAddTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(330, 108);
            this.Controls.Add(this.btnSaveTest);
            this.Controls.Add(this.btnCancelTest);
            this.Controls.Add(this.txtTestName);
            this.Controls.Add(this.lblTestName);
            this.Name = "frmAddTest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "AddTest";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmAddTest_FormClosing);
            this.Load += new System.EventHandler(this.AddTest_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTestName;
        private System.Windows.Forms.TextBox txtTestName;
        private System.Windows.Forms.Button btnCancelTest;
        private System.Windows.Forms.Button btnSaveTest;
    }
}