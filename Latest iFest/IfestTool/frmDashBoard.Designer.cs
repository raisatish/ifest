﻿using System;
using System.Windows.Forms;

namespace IfestTool
{
    partial class frmDashBoard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label lblschedulerrun;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDashBoard));
            this.cmsMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miSettings = new System.Windows.Forms.ToolStripMenuItem();
            this.miConfiguration = new System.Windows.Forms.ToolStripMenuItem();
            this.miReport = new System.Windows.Forms.ToolStripMenuItem();
            this.miClose = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.tabScheduler = new System.Windows.Forms.TabControl();
            this.tabConfigure = new System.Windows.Forms.TabPage();
            this.btnDeleteTestSymbol = new System.Windows.Forms.Button();
            this.btnAddTestSymbol = new System.Windows.Forms.Button();
            this.btnDeleteTest = new System.Windows.Forms.Button();
            this.btnAddTest = new System.Windows.Forms.Button();
            this.lvTests = new System.Windows.Forms.ListView();
            this.chTestName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lvClasses = new System.Windows.Forms.ListView();
            this.chCLassName = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabSchedule = new System.Windows.Forms.TabPage();
            this.btnSaveSchedulerSettings = new System.Windows.Forms.Button();
            this.btnCancelSchedulerSettings = new System.Windows.Forms.Button();
            this.pnlScheduler = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.pnlSchedulerInterval = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.cmbIntervals = new System.Windows.Forms.ComboBox();
            this.lblschedulerinterval = new System.Windows.Forms.Label();
            this.rdbschedulerdaily = new System.Windows.Forms.RadioButton();
            this.rdbschedulerhourly = new System.Windows.Forms.RadioButton();
            this.dateTimeescheduler = new System.Windows.Forms.DateTimePicker();
            this.lblschedulerStart = new System.Windows.Forms.Label();
            this.tabCodeBase = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label15 = new System.Windows.Forms.Label();
            this.txtgitauthenticationcode = new System.Windows.Forms.TextBox();
            this.lblgitauthenticationcode = new System.Windows.Forms.Label();
            this.txtgitclientsecret = new System.Windows.Forms.TextBox();
            this.lblgitClientSecret = new System.Windows.Forms.Label();
            this.lblgitClientId = new System.Windows.Forms.Label();
            this.txtgitClientId = new System.Windows.Forms.TextBox();
            this.txtgithubproduct = new System.Windows.Forms.TextBox();
            this.txtProjectName = new System.Windows.Forms.TextBox();
            this.lblProjectName = new System.Windows.Forms.Label();
            this.lblServer = new System.Windows.Forms.Label();
            this.lblCommitId = new System.Windows.Forms.Label();
            this.txtToken = new System.Windows.Forms.TextBox();
            this.lblToken = new System.Windows.Forms.Label();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.txtCommitId = new System.Windows.Forms.TextBox();
            this.cmbSource = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.btnSaveSettings = new System.Windows.Forms.Button();
            this.btnCancelCnfg = new System.Windows.Forms.Button();
            this.tabExecReports = new System.Windows.Forms.TabPage();
            this.lblNoReps = new System.Windows.Forms.Label();
            this.tvSchExecResults = new System.Windows.Forms.TreeView();
            this.imgListTV = new System.Windows.Forms.ImageList(this.components);
            this.tabTestExec = new System.Windows.Forms.TabPage();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnexecute = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtAzureEnvironment = new System.Windows.Forms.TextBox();
            this.lblAzureEnvironment = new System.Windows.Forms.Label();
            this.txtMTMenvironment = new System.Windows.Forms.TextBox();
            this.txtAzureconfigId = new System.Windows.Forms.TextBox();
            this.lblAzureconfigId = new System.Windows.Forms.Label();
            this.txtMTMconfigID = new System.Windows.Forms.TextBox();
            this.txtAzureSettingsID = new System.Windows.Forms.TextBox();
            this.lblAzureSettingsId = new System.Windows.Forms.Label();
            this.txtMTMsettingsID = new System.Windows.Forms.TextBox();
            this.txtAzuretestsuiteId = new System.Windows.Forms.TextBox();
            this.txtMTMtestsuiteId = new System.Windows.Forms.TextBox();
            this.lblAzuretestsuiteId = new System.Windows.Forms.Label();
            this.txtAzuretestplanId = new System.Windows.Forms.TextBox();
            this.lblAzuretestplanId = new System.Windows.Forms.Label();
            this.txtMTMtestplanId = new System.Windows.Forms.TextBox();
            this.lblMTMenvironment = new System.Windows.Forms.Label();
            this.lblMTMconfigId = new System.Windows.Forms.Label();
            this.lblMTMsettingsId = new System.Windows.Forms.Label();
            this.lblMTMsuiteId = new System.Windows.Forms.Label();
            this.lblMTMtestplanid = new System.Windows.Forms.Label();
            this.txtqtestPassword = new System.Windows.Forms.TextBox();
            this.lblqtestPassword = new System.Windows.Forms.Label();
            this.txtAzurePAT = new System.Windows.Forms.TextBox();
            this.lblAzurePAT = new System.Windows.Forms.Label();
            this.txtqtestUserName = new System.Windows.Forms.TextBox();
            this.lblqTestUSerName = new System.Windows.Forms.Label();
            this.txtazure = new System.Windows.Forms.TextBox();
            this.lblazureserver = new System.Windows.Forms.Label();
            this.txtMTMPassword = new System.Windows.Forms.TextBox();
            this.txtMTMUserName = new System.Windows.Forms.TextBox();
            this.lblMTMpassword = new System.Windows.Forms.Label();
            this.lblMTMUserName = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtqTest = new System.Windows.Forms.TextBox();
            this.lblqTestserver = new System.Windows.Forms.Label();
            this.txtmtm = new System.Windows.Forms.TextBox();
            this.lblmtmserver = new System.Windows.Forms.Label();
            this.cmbtestexecution = new System.Windows.Forms.ComboBox();
            this.lbltestrepositoty = new System.Windows.Forms.Label();
            this.tabTestResult = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnRun = new System.Windows.Forms.Button();
            this.dateTimePickerEnd = new System.Windows.Forms.DateTimePicker();
            this.label17 = new System.Windows.Forms.Label();
            this.dateTimePickerStart = new System.Windows.Forms.DateTimePicker();
            this.label16 = new System.Windows.Forms.Label();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.lblTestSatusGrid = new System.Windows.Forms.Label();
            this.cmbboxtestresults = new System.Windows.Forms.ComboBox();
            this.lblestCaseId = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pnlInterval = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbInterval = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.rdbDaily = new System.Windows.Forms.RadioButton();
            this.rdbHourly = new System.Windows.Forms.RadioButton();
            this.dtStartTime = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtChangeset = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtSrvUrl = new System.Windows.Forms.TextBox();
            this.lblSrvUrl = new System.Windows.Forms.Label();
            this.txtSrvToken = new System.Windows.Forms.TextBox();
            this.lblSrvToken = new System.Windows.Forms.Label();
            this.rbTFS = new System.Windows.Forms.RadioButton();
            this.lblSC = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            label2 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            lblschedulerrun = new System.Windows.Forms.Label();
            this.cmsMenu.SuspendLayout();
            this.tabScheduler.SuspendLayout();
            this.tabConfigure.SuspendLayout();
            this.tabSchedule.SuspendLayout();
            this.pnlScheduler.SuspendLayout();
            this.pnlSchedulerInterval.SuspendLayout();
            this.tabCodeBase.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabExecReports.SuspendLayout();
            this.tabTestExec.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabTestResult.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.pnlInterval.SuspendLayout();
            this.SuspendLayout();
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.ForeColor = System.Drawing.Color.Black;
            label2.Location = new System.Drawing.Point(15, 44);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(33, 15);
            label2.TabIndex = 13;
            label2.Text = "Run";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label6.ForeColor = System.Drawing.Color.Black;
            label6.Location = new System.Drawing.Point(15, 11);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(128, 15);
            label6.TabIndex = 20;
            label6.Text = "Scheduler Settings";
            // 
            // lblschedulerrun
            // 
            lblschedulerrun.AutoSize = true;
            lblschedulerrun.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            lblschedulerrun.ForeColor = System.Drawing.Color.Black;
            lblschedulerrun.Location = new System.Drawing.Point(224, 72);
            lblschedulerrun.Name = "lblschedulerrun";
            lblschedulerrun.Size = new System.Drawing.Size(29, 16);
            lblschedulerrun.TabIndex = 20;
            lblschedulerrun.Text = "Run";
            // 
            // cmsMenu
            // 
            this.cmsMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miSettings,
            this.miConfiguration,
            this.miReport,
            this.miClose});
            this.cmsMenu.Name = "cmsMenu";
            this.cmsMenu.Size = new System.Drawing.Size(128, 92);
            // 
            // miSettings
            // 
            this.miSettings.Name = "miSettings";
            this.miSettings.Size = new System.Drawing.Size(127, 22);
            this.miSettings.Text = "Settings";
            this.miSettings.Click += new System.EventHandler(this.miScheduler_Click);
            // 
            // miConfiguration
            // 
            this.miConfiguration.Name = "miConfiguration";
            this.miConfiguration.Size = new System.Drawing.Size(127, 22);
            this.miConfiguration.Text = "Mappings";
            this.miConfiguration.Click += new System.EventHandler(this.miConfiguration_Click);
            // 
            // miReport
            // 
            this.miReport.Name = "miReport";
            this.miReport.Size = new System.Drawing.Size(127, 22);
            this.miReport.Text = "History";
            this.miReport.Click += new System.EventHandler(this.miReport_Click);
            // 
            // miClose
            // 
            this.miClose.Name = "miClose";
            this.miClose.Size = new System.Drawing.Size(127, 22);
            this.miClose.Text = "Close";
            this.miClose.Click += new System.EventHandler(this.miClose_Click);
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.notifyIcon.BalloonTipText = "Intelligently Filter Schedule and Execute Tests";
            this.notifyIcon.BalloonTipTitle = "iFest";
            this.notifyIcon.ContextMenuStrip = this.cmsMenu;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "iFest";
            this.notifyIcon.Visible = true;
            // 
            // tabScheduler
            // 
            this.tabScheduler.Controls.Add(this.tabConfigure);
            this.tabScheduler.Controls.Add(this.tabSchedule);
            this.tabScheduler.Controls.Add(this.tabCodeBase);
            this.tabScheduler.Controls.Add(this.tabExecReports);
            this.tabScheduler.Controls.Add(this.tabTestExec);
            this.tabScheduler.Controls.Add(this.tabTestResult);
            this.tabScheduler.Location = new System.Drawing.Point(1, 0);
            this.tabScheduler.Name = "tabScheduler";
            this.tabScheduler.SelectedIndex = 0;
            this.tabScheduler.Size = new System.Drawing.Size(839, 436);
            this.tabScheduler.TabIndex = 1;
            this.tabScheduler.SelectedIndexChanged += new System.EventHandler(this.tcTab_SelectedIndexChanged);
            // 
            // tabConfigure
            // 
            this.tabConfigure.Controls.Add(this.btnDeleteTestSymbol);
            this.tabConfigure.Controls.Add(this.btnAddTestSymbol);
            this.tabConfigure.Controls.Add(this.btnDeleteTest);
            this.tabConfigure.Controls.Add(this.btnAddTest);
            this.tabConfigure.Controls.Add(this.lvTests);
            this.tabConfigure.Controls.Add(this.lvClasses);
            this.tabConfigure.Location = new System.Drawing.Point(4, 22);
            this.tabConfigure.Name = "tabConfigure";
            this.tabConfigure.Padding = new System.Windows.Forms.Padding(3);
            this.tabConfigure.Size = new System.Drawing.Size(831, 410);
            this.tabConfigure.TabIndex = 0;
            this.tabConfigure.Text = "Mappings";
            this.tabConfigure.UseVisualStyleBackColor = true;
            // 
            // btnDeleteTestSymbol
            // 
            this.btnDeleteTestSymbol.BackColor = System.Drawing.Color.Firebrick;
            this.btnDeleteTestSymbol.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeleteTestSymbol.ForeColor = System.Drawing.Color.FloralWhite;
            this.btnDeleteTestSymbol.Location = new System.Drawing.Point(749, 1);
            this.btnDeleteTestSymbol.Name = "btnDeleteTestSymbol";
            this.btnDeleteTestSymbol.Size = new System.Drawing.Size(30, 25);
            this.btnDeleteTestSymbol.TabIndex = 9;
            this.btnDeleteTestSymbol.Text = "-";
            this.btnDeleteTestSymbol.UseVisualStyleBackColor = false;
            this.btnDeleteTestSymbol.Click += new System.EventHandler(this.btnDeleteTest_Click);
            // 
            // btnAddTestSymbol
            // 
            this.btnAddTestSymbol.BackColor = System.Drawing.Color.Green;
            this.btnAddTestSymbol.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddTestSymbol.ForeColor = System.Drawing.Color.FloralWhite;
            this.btnAddTestSymbol.Location = new System.Drawing.Point(785, -1);
            this.btnAddTestSymbol.Name = "btnAddTestSymbol";
            this.btnAddTestSymbol.Size = new System.Drawing.Size(30, 26);
            this.btnAddTestSymbol.TabIndex = 8;
            this.btnAddTestSymbol.Text = "+";
            this.btnAddTestSymbol.UseVisualStyleBackColor = false;
            this.btnAddTestSymbol.Click += new System.EventHandler(this.btnAddTest_Click);
            // 
            // btnDeleteTest
            // 
            this.btnDeleteTest.Location = new System.Drawing.Point(588, 338);
            this.btnDeleteTest.Name = "btnDeleteTest";
            this.btnDeleteTest.Size = new System.Drawing.Size(75, 30);
            this.btnDeleteTest.TabIndex = 4;
            this.btnDeleteTest.Text = "Delete Test";
            this.btnDeleteTest.UseVisualStyleBackColor = true;
            this.btnDeleteTest.Visible = false;
            this.btnDeleteTest.Click += new System.EventHandler(this.btnDeleteTest_Click);
            // 
            // btnAddTest
            // 
            this.btnAddTest.Location = new System.Drawing.Point(690, 338);
            this.btnAddTest.Name = "btnAddTest";
            this.btnAddTest.Size = new System.Drawing.Size(79, 30);
            this.btnAddTest.TabIndex = 3;
            this.btnAddTest.Text = "Add Test";
            this.btnAddTest.UseVisualStyleBackColor = true;
            this.btnAddTest.Visible = false;
            this.btnAddTest.Click += new System.EventHandler(this.btnAddTest_Click);
            // 
            // lvTests
            // 
            this.lvTests.BackColor = System.Drawing.Color.Lavender;
            this.lvTests.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chTestName});
            this.lvTests.FullRowSelect = true;
            this.lvTests.GridLines = true;
            this.lvTests.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvTests.HideSelection = false;
            this.lvTests.Location = new System.Drawing.Point(521, 0);
            this.lvTests.MultiSelect = false;
            this.lvTests.Name = "lvTests";
            this.lvTests.ShowGroups = false;
            this.lvTests.Size = new System.Drawing.Size(304, 422);
            this.lvTests.TabIndex = 1;
            this.lvTests.UseCompatibleStateImageBehavior = false;
            this.lvTests.View = System.Windows.Forms.View.Details;
            this.lvTests.SelectedIndexChanged += new System.EventHandler(this.LvTests_SelectedIndexChanged);
            // 
            // chTestName
            // 
            this.chTestName.Text = "Test Name";
            this.chTestName.Width = 250;
            // 
            // lvClasses
            // 
            this.lvClasses.BackColor = System.Drawing.Color.LightSteelBlue;
            this.lvClasses.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.chCLassName});
            this.lvClasses.FullRowSelect = true;
            this.lvClasses.GridLines = true;
            this.lvClasses.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvClasses.HideSelection = false;
            this.lvClasses.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.lvClasses.Location = new System.Drawing.Point(3, 0);
            this.lvClasses.MultiSelect = false;
            this.lvClasses.Name = "lvClasses";
            this.lvClasses.Size = new System.Drawing.Size(518, 425);
            this.lvClasses.TabIndex = 0;
            this.lvClasses.UseCompatibleStateImageBehavior = false;
            this.lvClasses.View = System.Windows.Forms.View.Details;
            this.lvClasses.ItemSelectionChanged += new System.Windows.Forms.ListViewItemSelectionChangedEventHandler(this.lvClasses_ItemSelectionChanged);
            // 
            // chCLassName
            // 
            this.chCLassName.Text = "Class Name";
            this.chCLassName.Width = 355;
            // 
            // tabSchedule
            // 
            this.tabSchedule.Controls.Add(this.btnSaveSchedulerSettings);
            this.tabSchedule.Controls.Add(this.btnCancelSchedulerSettings);
            this.tabSchedule.Controls.Add(this.pnlScheduler);
            this.tabSchedule.Location = new System.Drawing.Point(4, 22);
            this.tabSchedule.Name = "tabSchedule";
            this.tabSchedule.Size = new System.Drawing.Size(831, 410);
            this.tabSchedule.TabIndex = 4;
            this.tabSchedule.Text = "Scheduler Settings";
            this.tabSchedule.UseVisualStyleBackColor = true;
            // 
            // btnSaveSchedulerSettings
            // 
            this.btnSaveSchedulerSettings.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.btnSaveSchedulerSettings.Image = global::IfestTool.Properties.Resources.save;
            this.btnSaveSchedulerSettings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaveSchedulerSettings.Location = new System.Drawing.Point(430, 373);
            this.btnSaveSchedulerSettings.Name = "btnSaveSchedulerSettings";
            this.btnSaveSchedulerSettings.Padding = new System.Windows.Forms.Padding(6, 3, 14, 4);
            this.btnSaveSchedulerSettings.Size = new System.Drawing.Size(85, 30);
            this.btnSaveSchedulerSettings.TabIndex = 11;
            this.btnSaveSchedulerSettings.Text = "Save";
            this.btnSaveSchedulerSettings.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSaveSchedulerSettings.UseVisualStyleBackColor = true;
            this.btnSaveSchedulerSettings.Click += new System.EventHandler(this.BtnSaveSchedulerSettings_Click);
            // 
            // btnCancelSchedulerSettings
            // 
            this.btnCancelSchedulerSettings.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.btnCancelSchedulerSettings.Image = global::IfestTool.Properties.Resources.reset;
            this.btnCancelSchedulerSettings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelSchedulerSettings.Location = new System.Drawing.Point(242, 373);
            this.btnCancelSchedulerSettings.Name = "btnCancelSchedulerSettings";
            this.btnCancelSchedulerSettings.Padding = new System.Windows.Forms.Padding(6, 3, 14, 4);
            this.btnCancelSchedulerSettings.Size = new System.Drawing.Size(85, 30);
            this.btnCancelSchedulerSettings.TabIndex = 10;
            this.btnCancelSchedulerSettings.Text = "Reset";
            this.btnCancelSchedulerSettings.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelSchedulerSettings.UseVisualStyleBackColor = true;
            this.btnCancelSchedulerSettings.Click += new System.EventHandler(this.BtnCancelSchedulerSettings_Click);
            // 
            // pnlScheduler
            // 
            this.pnlScheduler.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pnlScheduler.BackgroundImage")));
            this.pnlScheduler.Controls.Add(this.label12);
            this.pnlScheduler.Controls.Add(this.pnlSchedulerInterval);
            this.pnlScheduler.Controls.Add(this.rdbschedulerdaily);
            this.pnlScheduler.Controls.Add(this.rdbschedulerhourly);
            this.pnlScheduler.Controls.Add(lblschedulerrun);
            this.pnlScheduler.Controls.Add(this.dateTimeescheduler);
            this.pnlScheduler.Controls.Add(this.lblschedulerStart);
            this.pnlScheduler.Location = new System.Drawing.Point(-50, 0);
            this.pnlScheduler.Name = "pnlScheduler";
            this.pnlScheduler.Size = new System.Drawing.Size(881, 359);
            this.pnlScheduler.TabIndex = 0;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold);
            this.label12.ForeColor = System.Drawing.Color.Black;
            this.label12.Location = new System.Drawing.Point(315, 22);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(263, 18);
            this.label12.TabIndex = 24;
            this.label12.Text = "SCHEDULER  CONFIGURATION\r\n";
            // 
            // pnlSchedulerInterval
            // 
            this.pnlSchedulerInterval.Controls.Add(this.label13);
            this.pnlSchedulerInterval.Controls.Add(this.cmbIntervals);
            this.pnlSchedulerInterval.Controls.Add(this.lblschedulerinterval);
            this.pnlSchedulerInterval.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.pnlSchedulerInterval.ForeColor = System.Drawing.Color.FloralWhite;
            this.pnlSchedulerInterval.Location = new System.Drawing.Point(148, 181);
            this.pnlSchedulerInterval.Name = "pnlSchedulerInterval";
            this.pnlSchedulerInterval.Size = new System.Drawing.Size(417, 54);
            this.pnlSchedulerInterval.TabIndex = 23;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.label13.ForeColor = System.Drawing.Color.Black;
            this.label13.Location = new System.Drawing.Point(272, 13);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(38, 16);
            this.label13.TabIndex = 8;
            this.label13.Text = "(Hrs.)";
            // 
            // cmbIntervals
            // 
            this.cmbIntervals.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIntervals.FormattingEnabled = true;
            this.cmbIntervals.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.cmbIntervals.Location = new System.Drawing.Point(161, 7);
            this.cmbIntervals.Name = "cmbIntervals";
            this.cmbIntervals.Size = new System.Drawing.Size(99, 24);
            this.cmbIntervals.TabIndex = 7;
            // 
            // lblschedulerinterval
            // 
            this.lblschedulerinterval.AutoSize = true;
            this.lblschedulerinterval.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblschedulerinterval.ForeColor = System.Drawing.Color.Black;
            this.lblschedulerinterval.Location = new System.Drawing.Point(72, 7);
            this.lblschedulerinterval.Name = "lblschedulerinterval";
            this.lblschedulerinterval.Size = new System.Drawing.Size(52, 16);
            this.lblschedulerinterval.TabIndex = 6;
            this.lblschedulerinterval.Text = "Interval";
            // 
            // rdbschedulerdaily
            // 
            this.rdbschedulerdaily.AutoSize = true;
            this.rdbschedulerdaily.Checked = true;
            this.rdbschedulerdaily.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.rdbschedulerdaily.ForeColor = System.Drawing.Color.Black;
            this.rdbschedulerdaily.Location = new System.Drawing.Point(322, 73);
            this.rdbschedulerdaily.Name = "rdbschedulerdaily";
            this.rdbschedulerdaily.Size = new System.Drawing.Size(55, 20);
            this.rdbschedulerdaily.TabIndex = 18;
            this.rdbschedulerdaily.TabStop = true;
            this.rdbschedulerdaily.Text = "Daily";
            this.rdbschedulerdaily.UseVisualStyleBackColor = true;
            // 
            // rdbschedulerhourly
            // 
            this.rdbschedulerhourly.AutoSize = true;
            this.rdbschedulerhourly.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.rdbschedulerhourly.ForeColor = System.Drawing.Color.Black;
            this.rdbschedulerhourly.Location = new System.Drawing.Point(390, 73);
            this.rdbschedulerhourly.Name = "rdbschedulerhourly";
            this.rdbschedulerhourly.Size = new System.Drawing.Size(63, 20);
            this.rdbschedulerhourly.TabIndex = 19;
            this.rdbschedulerhourly.Text = "Hourly";
            this.rdbschedulerhourly.UseVisualStyleBackColor = true;
            // 
            // dateTimeescheduler
            // 
            this.dateTimeescheduler.CustomFormat = "HH:mm";
            this.dateTimeescheduler.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimeescheduler.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeescheduler.Location = new System.Drawing.Point(316, 123);
            this.dateTimeescheduler.Name = "dateTimeescheduler";
            this.dateTimeescheduler.ShowUpDown = true;
            this.dateTimeescheduler.Size = new System.Drawing.Size(96, 22);
            this.dateTimeescheduler.TabIndex = 22;
            // 
            // lblschedulerStart
            // 
            this.lblschedulerStart.AutoSize = true;
            this.lblschedulerStart.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblschedulerStart.ForeColor = System.Drawing.Color.Black;
            this.lblschedulerStart.Location = new System.Drawing.Point(224, 123);
            this.lblschedulerStart.Name = "lblschedulerStart";
            this.lblschedulerStart.Size = new System.Drawing.Size(66, 16);
            this.lblschedulerStart.TabIndex = 21;
            this.lblschedulerStart.Text = "Start Time:";
            // 
            // tabCodeBase
            // 
            this.tabCodeBase.Controls.Add(this.panel2);
            this.tabCodeBase.Controls.Add(this.btnSaveSettings);
            this.tabCodeBase.Controls.Add(this.btnCancelCnfg);
            this.tabCodeBase.Location = new System.Drawing.Point(4, 22);
            this.tabCodeBase.Name = "tabCodeBase";
            this.tabCodeBase.Padding = new System.Windows.Forms.Padding(3);
            this.tabCodeBase.Size = new System.Drawing.Size(831, 410);
            this.tabCodeBase.TabIndex = 1;
            this.tabCodeBase.Text = "Code Base Settings";
            this.tabCodeBase.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel2.BackgroundImage")));
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.label15);
            this.panel2.Controls.Add(this.txtgitauthenticationcode);
            this.panel2.Controls.Add(this.lblgitauthenticationcode);
            this.panel2.Controls.Add(this.txtgitclientsecret);
            this.panel2.Controls.Add(this.lblgitClientSecret);
            this.panel2.Controls.Add(this.lblgitClientId);
            this.panel2.Controls.Add(this.txtgitClientId);
            this.panel2.Controls.Add(this.txtgithubproduct);
            this.panel2.Controls.Add(this.txtProjectName);
            this.panel2.Controls.Add(this.lblProjectName);
            this.panel2.Controls.Add(this.lblServer);
            this.panel2.Controls.Add(this.lblCommitId);
            this.panel2.Controls.Add(this.txtToken);
            this.panel2.Controls.Add(this.lblToken);
            this.panel2.Controls.Add(this.txtServer);
            this.panel2.Controls.Add(this.txtCommitId);
            this.panel2.Controls.Add(this.cmbSource);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.panel2.Location = new System.Drawing.Point(-50, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(878, 359);
            this.panel2.TabIndex = 18;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Black;
            this.label15.Location = new System.Drawing.Point(292, 32);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(307, 36);
            this.label15.TabIndex = 48;
            this.label15.Text = "CODE REPISTORY CONFIGURATION\r\n\r\n";
            this.label15.Click += new System.EventHandler(this.label15_Click);
            // 
            // txtgitauthenticationcode
            // 
            this.txtgitauthenticationcode.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgitauthenticationcode.Location = new System.Drawing.Point(320, 250);
            this.txtgitauthenticationcode.Name = "txtgitauthenticationcode";
            this.txtgitauthenticationcode.Size = new System.Drawing.Size(279, 22);
            this.txtgitauthenticationcode.TabIndex = 47;
            this.txtgitauthenticationcode.Visible = false;
            // 
            // lblgitauthenticationcode
            // 
            this.lblgitauthenticationcode.AutoSize = true;
            this.lblgitauthenticationcode.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblgitauthenticationcode.ForeColor = System.Drawing.Color.Black;
            this.lblgitauthenticationcode.Location = new System.Drawing.Point(161, 256);
            this.lblgitauthenticationcode.Name = "lblgitauthenticationcode";
            this.lblgitauthenticationcode.Size = new System.Drawing.Size(128, 16);
            this.lblgitauthenticationcode.TabIndex = 46;
            this.lblgitauthenticationcode.Text = "Authentication Code";
            this.lblgitauthenticationcode.Visible = false;
            // 
            // txtgitclientsecret
            // 
            this.txtgitclientsecret.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgitclientsecret.Location = new System.Drawing.Point(320, 213);
            this.txtgitclientsecret.Name = "txtgitclientsecret";
            this.txtgitclientsecret.Size = new System.Drawing.Size(279, 22);
            this.txtgitclientsecret.TabIndex = 45;
            this.txtgitclientsecret.Visible = false;
            // 
            // lblgitClientSecret
            // 
            this.lblgitClientSecret.AutoSize = true;
            this.lblgitClientSecret.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblgitClientSecret.ForeColor = System.Drawing.Color.Black;
            this.lblgitClientSecret.Location = new System.Drawing.Point(161, 216);
            this.lblgitClientSecret.Name = "lblgitClientSecret";
            this.lblgitClientSecret.Size = new System.Drawing.Size(81, 16);
            this.lblgitClientSecret.TabIndex = 44;
            this.lblgitClientSecret.Text = "Client Secret";
            this.lblgitClientSecret.Visible = false;
            // 
            // lblgitClientId
            // 
            this.lblgitClientId.AutoSize = true;
            this.lblgitClientId.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblgitClientId.ForeColor = System.Drawing.Color.Black;
            this.lblgitClientId.Location = new System.Drawing.Point(161, 173);
            this.lblgitClientId.Name = "lblgitClientId";
            this.lblgitClientId.Size = new System.Drawing.Size(53, 16);
            this.lblgitClientId.TabIndex = 43;
            this.lblgitClientId.Text = "ClientId";
            this.lblgitClientId.Visible = false;
            // 
            // txtgitClientId
            // 
            this.txtgitClientId.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgitClientId.Location = new System.Drawing.Point(320, 173);
            this.txtgitClientId.Name = "txtgitClientId";
            this.txtgitClientId.Size = new System.Drawing.Size(279, 22);
            this.txtgitClientId.TabIndex = 42;
            this.txtgitClientId.Visible = false;
            // 
            // txtgithubproduct
            // 
            this.txtgithubproduct.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgithubproduct.Location = new System.Drawing.Point(320, 130);
            this.txtgithubproduct.Name = "txtgithubproduct";
            this.txtgithubproduct.Size = new System.Drawing.Size(279, 22);
            this.txtgithubproduct.TabIndex = 40;
            this.txtgithubproduct.Visible = false;
            // 
            // txtProjectName
            // 
            this.txtProjectName.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtProjectName.Location = new System.Drawing.Point(320, 250);
            this.txtProjectName.Name = "txtProjectName";
            this.txtProjectName.Size = new System.Drawing.Size(279, 22);
            this.txtProjectName.TabIndex = 39;
            this.txtProjectName.Visible = false;
            // 
            // lblProjectName
            // 
            this.lblProjectName.AutoSize = true;
            this.lblProjectName.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblProjectName.ForeColor = System.Drawing.Color.Black;
            this.lblProjectName.Location = new System.Drawing.Point(161, 256);
            this.lblProjectName.Name = "lblProjectName";
            this.lblProjectName.Size = new System.Drawing.Size(88, 16);
            this.lblProjectName.TabIndex = 38;
            this.lblProjectName.Text = "Project Name";
            this.lblProjectName.Visible = false;
            // 
            // lblServer
            // 
            this.lblServer.AutoSize = true;
            this.lblServer.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblServer.ForeColor = System.Drawing.Color.Black;
            this.lblServer.Location = new System.Drawing.Point(161, 173);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(45, 16);
            this.lblServer.TabIndex = 37;
            this.lblServer.Text = "Server";
            this.lblServer.Visible = false;
            // 
            // lblCommitId
            // 
            this.lblCommitId.AutoSize = true;
            this.lblCommitId.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblCommitId.ForeColor = System.Drawing.Color.Black;
            this.lblCommitId.Location = new System.Drawing.Point(161, 136);
            this.lblCommitId.Name = "lblCommitId";
            this.lblCommitId.Size = new System.Drawing.Size(65, 16);
            this.lblCommitId.TabIndex = 36;
            this.lblCommitId.Text = "CommitId";
            this.lblCommitId.Visible = false;
            // 
            // txtToken
            // 
            this.txtToken.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtToken.Location = new System.Drawing.Point(320, 213);
            this.txtToken.Name = "txtToken";
            this.txtToken.Size = new System.Drawing.Size(279, 22);
            this.txtToken.TabIndex = 35;
            this.txtToken.Visible = false;
            // 
            // lblToken
            // 
            this.lblToken.AutoSize = true;
            this.lblToken.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblToken.ForeColor = System.Drawing.Color.Black;
            this.lblToken.Location = new System.Drawing.Point(161, 216);
            this.lblToken.Name = "lblToken";
            this.lblToken.Size = new System.Drawing.Size(44, 16);
            this.lblToken.TabIndex = 34;
            this.lblToken.Text = "Token";
            this.lblToken.Visible = false;
            // 
            // txtServer
            // 
            this.txtServer.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtServer.Location = new System.Drawing.Point(320, 173);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(279, 22);
            this.txtServer.TabIndex = 33;
            this.txtServer.Visible = false;
            // 
            // txtCommitId
            // 
            this.txtCommitId.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCommitId.Location = new System.Drawing.Point(320, 130);
            this.txtCommitId.Name = "txtCommitId";
            this.txtCommitId.Size = new System.Drawing.Size(279, 22);
            this.txtCommitId.TabIndex = 31;
            this.txtCommitId.Visible = false;
            // 
            // cmbSource
            // 
            this.cmbSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSource.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbSource.FormattingEnabled = true;
            this.cmbSource.Location = new System.Drawing.Point(320, 87);
            this.cmbSource.Name = "cmbSource";
            this.cmbSource.Size = new System.Drawing.Size(279, 24);
            this.cmbSource.TabIndex = 29;
            this.cmbSource.SelectedIndexChanged += new System.EventHandler(this.cmbSource_SelectedIndexChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.label10.ForeColor = System.Drawing.Color.Black;
            this.label10.Location = new System.Drawing.Point(161, 88);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(88, 16);
            this.label10.TabIndex = 28;
            this.label10.Text = "Select Source";
            // 
            // btnSaveSettings
            // 
            this.btnSaveSettings.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveSettings.Image = ((System.Drawing.Image)(resources.GetObject("btnSaveSettings.Image")));
            this.btnSaveSettings.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSaveSettings.Location = new System.Drawing.Point(446, 374);
            this.btnSaveSettings.Name = "btnSaveSettings";
            this.btnSaveSettings.Padding = new System.Windows.Forms.Padding(6, 3, 14, 4);
            this.btnSaveSettings.Size = new System.Drawing.Size(85, 30);
            this.btnSaveSettings.TabIndex = 9;
            this.btnSaveSettings.Text = "Save";
            this.btnSaveSettings.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSaveSettings.UseVisualStyleBackColor = true;
            this.btnSaveSettings.Click += new System.EventHandler(this.btnSaveSettings_Click);
            // 
            // btnCancelCnfg
            // 
            this.btnCancelCnfg.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelCnfg.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelCnfg.Image")));
            this.btnCancelCnfg.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelCnfg.Location = new System.Drawing.Point(245, 374);
            this.btnCancelCnfg.Name = "btnCancelCnfg";
            this.btnCancelCnfg.Padding = new System.Windows.Forms.Padding(6, 3, 14, 4);
            this.btnCancelCnfg.Size = new System.Drawing.Size(85, 30);
            this.btnCancelCnfg.TabIndex = 9;
            this.btnCancelCnfg.Text = "Reset";
            this.btnCancelCnfg.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelCnfg.UseVisualStyleBackColor = true;
            this.btnCancelCnfg.Click += new System.EventHandler(this.btnCancelCnfg_Click);
            // 
            // tabExecReports
            // 
            this.tabExecReports.Controls.Add(this.lblNoReps);
            this.tabExecReports.Controls.Add(this.tvSchExecResults);
            this.tabExecReports.Location = new System.Drawing.Point(4, 22);
            this.tabExecReports.Name = "tabExecReports";
            this.tabExecReports.Size = new System.Drawing.Size(831, 410);
            this.tabExecReports.TabIndex = 2;
            this.tabExecReports.Text = "History";
            this.tabExecReports.UseVisualStyleBackColor = true;
            // 
            // lblNoReps
            // 
            this.lblNoReps.AutoSize = true;
            this.lblNoReps.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblNoReps.ForeColor = System.Drawing.Color.Red;
            this.lblNoReps.Location = new System.Drawing.Point(19, 9);
            this.lblNoReps.Name = "lblNoReps";
            this.lblNoReps.Size = new System.Drawing.Size(146, 18);
            this.lblNoReps.TabIndex = 1;
            this.lblNoReps.Text = "Reports not available";
            this.lblNoReps.Visible = false;
            // 
            // tvSchExecResults
            // 
            this.tvSchExecResults.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tvSchExecResults.ImageIndex = 0;
            this.tvSchExecResults.ImageList = this.imgListTV;
            this.tvSchExecResults.Location = new System.Drawing.Point(0, 28);
            this.tvSchExecResults.Name = "tvSchExecResults";
            this.tvSchExecResults.SelectedImageIndex = 0;
            this.tvSchExecResults.Size = new System.Drawing.Size(754, 383);
            this.tvSchExecResults.TabIndex = 0;
            // 
            // imgListTV
            // 
            this.imgListTV.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgListTV.ImageStream")));
            this.imgListTV.TransparentColor = System.Drawing.Color.Transparent;
            this.imgListTV.Images.SetKeyName(0, "csfile.png");
            this.imgListTV.Images.SetKeyName(1, "datetime.png");
            this.imgListTV.Images.SetKeyName(2, "tests_big.png");
            // 
            // tabTestExec
            // 
            this.tabTestExec.Controls.Add(this.btnCancel);
            this.tabTestExec.Controls.Add(this.btnSave);
            this.tabTestExec.Controls.Add(this.btnexecute);
            this.tabTestExec.Controls.Add(this.panel3);
            this.tabTestExec.Location = new System.Drawing.Point(4, 22);
            this.tabTestExec.Name = "tabTestExec";
            this.tabTestExec.Size = new System.Drawing.Size(831, 410);
            this.tabTestExec.TabIndex = 3;
            this.tabTestExec.Text = "Test Server Setting";
            this.tabTestExec.UseVisualStyleBackColor = true;
            // 
            // btnCancel
            // 
            this.btnCancel.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.btnCancel.Image = global::IfestTool.Properties.Resources.reset;
            this.btnCancel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancel.Location = new System.Drawing.Point(187, 368);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Padding = new System.Windows.Forms.Padding(6, 3, 14, 4);
            this.btnCancel.Size = new System.Drawing.Size(85, 30);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Reset";
            this.btnCancel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.BtnCancel_Click);
            // 
            // btnSave
            // 
            this.btnSave.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.btnSave.Image = global::IfestTool.Properties.Resources.save;
            this.btnSave.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSave.Location = new System.Drawing.Point(348, 368);
            this.btnSave.Name = "btnSave";
            this.btnSave.Padding = new System.Windows.Forms.Padding(6, 3, 14, 4);
            this.btnSave.Size = new System.Drawing.Size(85, 30);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save";
            this.btnSave.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.BtnSave_Click);
            // 
            // btnexecute
            // 
            this.btnexecute.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.btnexecute.Image = global::IfestTool.Properties.Resources.run;
            this.btnexecute.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnexecute.Location = new System.Drawing.Point(500, 368);
            this.btnexecute.Name = "btnexecute";
            this.btnexecute.Padding = new System.Windows.Forms.Padding(6, 3, 25, 4);
            this.btnexecute.Size = new System.Drawing.Size(85, 30);
            this.btnexecute.TabIndex = 9;
            this.btnexecute.Text = "Run";
            this.btnexecute.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnexecute.UseVisualStyleBackColor = true;
            this.btnexecute.Click += new System.EventHandler(this.Btnexecute_Click);
            // 
            // panel3
            // 
            this.panel3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel3.BackgroundImage")));
            this.panel3.Controls.Add(this.txtAzureEnvironment);
            this.panel3.Controls.Add(this.lblAzureEnvironment);
            this.panel3.Controls.Add(this.txtMTMenvironment);
            this.panel3.Controls.Add(this.txtAzureconfigId);
            this.panel3.Controls.Add(this.lblAzureconfigId);
            this.panel3.Controls.Add(this.txtMTMconfigID);
            this.panel3.Controls.Add(this.txtAzureSettingsID);
            this.panel3.Controls.Add(this.lblAzureSettingsId);
            this.panel3.Controls.Add(this.txtMTMsettingsID);
            this.panel3.Controls.Add(this.txtAzuretestsuiteId);
            this.panel3.Controls.Add(this.txtMTMtestsuiteId);
            this.panel3.Controls.Add(this.lblAzuretestsuiteId);
            this.panel3.Controls.Add(this.txtAzuretestplanId);
            this.panel3.Controls.Add(this.lblAzuretestplanId);
            this.panel3.Controls.Add(this.txtMTMtestplanId);
            this.panel3.Controls.Add(this.lblMTMenvironment);
            this.panel3.Controls.Add(this.lblMTMconfigId);
            this.panel3.Controls.Add(this.lblMTMsettingsId);
            this.panel3.Controls.Add(this.lblMTMsuiteId);
            this.panel3.Controls.Add(this.lblMTMtestplanid);
            this.panel3.Controls.Add(this.txtqtestPassword);
            this.panel3.Controls.Add(this.lblqtestPassword);
            this.panel3.Controls.Add(this.txtAzurePAT);
            this.panel3.Controls.Add(this.lblAzurePAT);
            this.panel3.Controls.Add(this.txtqtestUserName);
            this.panel3.Controls.Add(this.lblqTestUSerName);
            this.panel3.Controls.Add(this.txtazure);
            this.panel3.Controls.Add(this.lblazureserver);
            this.panel3.Controls.Add(this.txtMTMPassword);
            this.panel3.Controls.Add(this.txtMTMUserName);
            this.panel3.Controls.Add(this.lblMTMpassword);
            this.panel3.Controls.Add(this.lblMTMUserName);
            this.panel3.Controls.Add(this.label11);
            this.panel3.Controls.Add(this.txtqTest);
            this.panel3.Controls.Add(this.lblqTestserver);
            this.panel3.Controls.Add(this.txtmtm);
            this.panel3.Controls.Add(this.lblmtmserver);
            this.panel3.Controls.Add(this.cmbtestexecution);
            this.panel3.Controls.Add(this.lbltestrepositoty);
            this.panel3.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.panel3.Location = new System.Drawing.Point(-50, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(881, 356);
            this.panel3.TabIndex = 0;
            this.panel3.Paint += new System.Windows.Forms.PaintEventHandler(this.panel3_Paint);
            // 
            // txtAzureEnvironment
            // 
            this.txtAzureEnvironment.Location = new System.Drawing.Point(251, 287);
            this.txtAzureEnvironment.Name = "txtAzureEnvironment";
            this.txtAzureEnvironment.Size = new System.Drawing.Size(279, 22);
            this.txtAzureEnvironment.TabIndex = 76;
            // 
            // lblAzureEnvironment
            // 
            this.lblAzureEnvironment.AutoSize = true;
            this.lblAzureEnvironment.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblAzureEnvironment.ForeColor = System.Drawing.Color.Black;
            this.lblAzureEnvironment.Location = new System.Drawing.Point(113, 284);
            this.lblAzureEnvironment.Name = "lblAzureEnvironment";
            this.lblAzureEnvironment.Size = new System.Drawing.Size(118, 16);
            this.lblAzureEnvironment.TabIndex = 75;
            this.lblAzureEnvironment.Text = "Environment Name";
            // 
            // txtMTMenvironment
            // 
            this.txtMTMenvironment.Location = new System.Drawing.Point(252, 312);
            this.txtMTMenvironment.Multiline = true;
            this.txtMTMenvironment.Name = "txtMTMenvironment";
            this.txtMTMenvironment.Size = new System.Drawing.Size(279, 22);
            this.txtMTMenvironment.TabIndex = 74;
            // 
            // txtAzureconfigId
            // 
            this.txtAzureconfigId.Location = new System.Drawing.Point(251, 259);
            this.txtAzureconfigId.Name = "txtAzureconfigId";
            this.txtAzureconfigId.Size = new System.Drawing.Size(279, 22);
            this.txtAzureconfigId.TabIndex = 73;
            // 
            // lblAzureconfigId
            // 
            this.lblAzureconfigId.AutoSize = true;
            this.lblAzureconfigId.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblAzureconfigId.ForeColor = System.Drawing.Color.Black;
            this.lblAzureconfigId.Location = new System.Drawing.Point(113, 258);
            this.lblAzureconfigId.Name = "lblAzureconfigId";
            this.lblAzureconfigId.Size = new System.Drawing.Size(101, 16);
            this.lblAzureconfigId.TabIndex = 72;
            this.lblAzureconfigId.Text = "Configuration ID";
            // 
            // txtMTMconfigID
            // 
            this.txtMTMconfigID.Location = new System.Drawing.Point(252, 284);
            this.txtMTMconfigID.Name = "txtMTMconfigID";
            this.txtMTMconfigID.Size = new System.Drawing.Size(279, 22);
            this.txtMTMconfigID.TabIndex = 71;
            // 
            // txtAzureSettingsID
            // 
            this.txtAzureSettingsID.Location = new System.Drawing.Point(251, 231);
            this.txtAzureSettingsID.Name = "txtAzureSettingsID";
            this.txtAzureSettingsID.Size = new System.Drawing.Size(279, 22);
            this.txtAzureSettingsID.TabIndex = 70;
            // 
            // lblAzureSettingsId
            // 
            this.lblAzureSettingsId.AutoSize = true;
            this.lblAzureSettingsId.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblAzureSettingsId.ForeColor = System.Drawing.Color.Black;
            this.lblAzureSettingsId.Location = new System.Drawing.Point(113, 231);
            this.lblAzureSettingsId.Name = "lblAzureSettingsId";
            this.lblAzureSettingsId.Size = new System.Drawing.Size(88, 16);
            this.lblAzureSettingsId.TabIndex = 69;
            this.lblAzureSettingsId.Text = "Test SettingsId";
            // 
            // txtMTMsettingsID
            // 
            this.txtMTMsettingsID.Location = new System.Drawing.Point(252, 258);
            this.txtMTMsettingsID.Name = "txtMTMsettingsID";
            this.txtMTMsettingsID.Size = new System.Drawing.Size(279, 22);
            this.txtMTMsettingsID.TabIndex = 68;
            // 
            // txtAzuretestsuiteId
            // 
            this.txtAzuretestsuiteId.Location = new System.Drawing.Point(252, 199);
            this.txtAzuretestsuiteId.Name = "txtAzuretestsuiteId";
            this.txtAzuretestsuiteId.Size = new System.Drawing.Size(279, 22);
            this.txtAzuretestsuiteId.TabIndex = 67;
            // 
            // txtMTMtestsuiteId
            // 
            this.txtMTMtestsuiteId.Location = new System.Drawing.Point(252, 229);
            this.txtMTMtestsuiteId.Name = "txtMTMtestsuiteId";
            this.txtMTMtestsuiteId.Size = new System.Drawing.Size(279, 22);
            this.txtMTMtestsuiteId.TabIndex = 66;
            // 
            // lblAzuretestsuiteId
            // 
            this.lblAzuretestsuiteId.AutoSize = true;
            this.lblAzuretestsuiteId.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblAzuretestsuiteId.ForeColor = System.Drawing.Color.Black;
            this.lblAzuretestsuiteId.Location = new System.Drawing.Point(113, 202);
            this.lblAzuretestsuiteId.Name = "lblAzuretestsuiteId";
            this.lblAzuretestsuiteId.Size = new System.Drawing.Size(72, 16);
            this.lblAzuretestsuiteId.TabIndex = 65;
            this.lblAzuretestsuiteId.Text = "Test SuiteId";
            // 
            // txtAzuretestplanId
            // 
            this.txtAzuretestplanId.Location = new System.Drawing.Point(252, 171);
            this.txtAzuretestplanId.Name = "txtAzuretestplanId";
            this.txtAzuretestplanId.Size = new System.Drawing.Size(279, 22);
            this.txtAzuretestplanId.TabIndex = 64;
            // 
            // lblAzuretestplanId
            // 
            this.lblAzuretestplanId.AutoSize = true;
            this.lblAzuretestplanId.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblAzuretestplanId.ForeColor = System.Drawing.Color.Black;
            this.lblAzuretestplanId.Location = new System.Drawing.Point(113, 174);
            this.lblAzuretestplanId.Name = "lblAzuretestplanId";
            this.lblAzuretestplanId.Size = new System.Drawing.Size(70, 16);
            this.lblAzuretestplanId.TabIndex = 63;
            this.lblAzuretestplanId.Text = "Test PlanId";
            // 
            // txtMTMtestplanId
            // 
            this.txtMTMtestplanId.Location = new System.Drawing.Point(251, 202);
            this.txtMTMtestplanId.Name = "txtMTMtestplanId";
            this.txtMTMtestplanId.Size = new System.Drawing.Size(279, 22);
            this.txtMTMtestplanId.TabIndex = 62;
            // 
            // lblMTMenvironment
            // 
            this.lblMTMenvironment.AutoSize = true;
            this.lblMTMenvironment.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblMTMenvironment.ForeColor = System.Drawing.Color.Black;
            this.lblMTMenvironment.Location = new System.Drawing.Point(113, 312);
            this.lblMTMenvironment.Name = "lblMTMenvironment";
            this.lblMTMenvironment.Size = new System.Drawing.Size(118, 16);
            this.lblMTMenvironment.TabIndex = 61;
            this.lblMTMenvironment.Text = "Environment Name";
            // 
            // lblMTMconfigId
            // 
            this.lblMTMconfigId.AutoSize = true;
            this.lblMTMconfigId.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblMTMconfigId.ForeColor = System.Drawing.Color.Black;
            this.lblMTMconfigId.Location = new System.Drawing.Point(113, 284);
            this.lblMTMconfigId.Name = "lblMTMconfigId";
            this.lblMTMconfigId.Size = new System.Drawing.Size(101, 16);
            this.lblMTMconfigId.TabIndex = 60;
            this.lblMTMconfigId.Text = "Configuration ID";
            // 
            // lblMTMsettingsId
            // 
            this.lblMTMsettingsId.AutoSize = true;
            this.lblMTMsettingsId.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblMTMsettingsId.ForeColor = System.Drawing.Color.Black;
            this.lblMTMsettingsId.Location = new System.Drawing.Point(112, 258);
            this.lblMTMsettingsId.Name = "lblMTMsettingsId";
            this.lblMTMsettingsId.Size = new System.Drawing.Size(88, 16);
            this.lblMTMsettingsId.TabIndex = 59;
            this.lblMTMsettingsId.Text = "Test SettingsId";
            // 
            // lblMTMsuiteId
            // 
            this.lblMTMsuiteId.AutoSize = true;
            this.lblMTMsuiteId.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblMTMsuiteId.ForeColor = System.Drawing.Color.Black;
            this.lblMTMsuiteId.Location = new System.Drawing.Point(113, 228);
            this.lblMTMsuiteId.Name = "lblMTMsuiteId";
            this.lblMTMsuiteId.Size = new System.Drawing.Size(72, 16);
            this.lblMTMsuiteId.TabIndex = 58;
            this.lblMTMsuiteId.Text = "Test SuiteId";
            this.lblMTMsuiteId.Click += new System.EventHandler(this.lblMTMsuiteId_Click);
            // 
            // lblMTMtestplanid
            // 
            this.lblMTMtestplanid.AutoSize = true;
            this.lblMTMtestplanid.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblMTMtestplanid.ForeColor = System.Drawing.Color.Black;
            this.lblMTMtestplanid.Location = new System.Drawing.Point(112, 200);
            this.lblMTMtestplanid.Name = "lblMTMtestplanid";
            this.lblMTMtestplanid.Size = new System.Drawing.Size(70, 16);
            this.lblMTMtestplanid.TabIndex = 57;
            this.lblMTMtestplanid.Text = "Test PlanId";
            this.lblMTMtestplanid.Click += new System.EventHandler(this.lblMTMtestplanid_Click);
            // 
            // txtqtestPassword
            // 
            this.txtqtestPassword.Location = new System.Drawing.Point(252, 173);
            this.txtqtestPassword.Name = "txtqtestPassword";
            this.txtqtestPassword.PasswordChar = '*';
            this.txtqtestPassword.Size = new System.Drawing.Size(279, 22);
            this.txtqtestPassword.TabIndex = 56;
            this.txtqtestPassword.UseSystemPasswordChar = true;
            // 
            // lblqtestPassword
            // 
            this.lblqtestPassword.AutoSize = true;
            this.lblqtestPassword.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblqtestPassword.ForeColor = System.Drawing.Color.Black;
            this.lblqtestPassword.Location = new System.Drawing.Point(113, 173);
            this.lblqtestPassword.Name = "lblqtestPassword";
            this.lblqtestPassword.Size = new System.Drawing.Size(63, 16);
            this.lblqtestPassword.TabIndex = 55;
            this.lblqtestPassword.Text = "Password";
            this.lblqtestPassword.Click += new System.EventHandler(this.lblqtestPassword_Click);
            // 
            // txtAzurePAT
            // 
            this.txtAzurePAT.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.txtAzurePAT.Location = new System.Drawing.Point(251, 144);
            this.txtAzurePAT.Name = "txtAzurePAT";
            this.txtAzurePAT.Size = new System.Drawing.Size(279, 22);
            this.txtAzurePAT.TabIndex = 54;
            this.txtAzurePAT.TextChanged += new System.EventHandler(this.txtAzurePAT_TextChanged);
            // 
            // lblAzurePAT
            // 
            this.lblAzurePAT.AutoSize = true;
            this.lblAzurePAT.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblAzurePAT.ForeColor = System.Drawing.Color.Black;
            this.lblAzurePAT.Location = new System.Drawing.Point(113, 146);
            this.lblAzurePAT.Name = "lblAzurePAT";
            this.lblAzurePAT.Size = new System.Drawing.Size(30, 16);
            this.lblAzurePAT.TabIndex = 53;
            this.lblAzurePAT.Text = "PAT";
            this.lblAzurePAT.Click += new System.EventHandler(this.lblAzurePAT_Click);
            // 
            // txtqtestUserName
            // 
            this.txtqtestUserName.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.txtqtestUserName.Location = new System.Drawing.Point(252, 146);
            this.txtqtestUserName.Name = "txtqtestUserName";
            this.txtqtestUserName.Size = new System.Drawing.Size(279, 22);
            this.txtqtestUserName.TabIndex = 52;
            this.txtqtestUserName.TextChanged += new System.EventHandler(this.txtqtestUserName_TextChanged);
            // 
            // lblqTestUSerName
            // 
            this.lblqTestUSerName.AutoSize = true;
            this.lblqTestUSerName.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblqTestUSerName.ForeColor = System.Drawing.Color.Black;
            this.lblqTestUSerName.Location = new System.Drawing.Point(113, 148);
            this.lblqTestUSerName.Name = "lblqTestUSerName";
            this.lblqTestUSerName.Size = new System.Drawing.Size(69, 16);
            this.lblqTestUSerName.TabIndex = 51;
            this.lblqTestUSerName.Text = "UserName";
            this.lblqTestUSerName.Click += new System.EventHandler(this.lblqTestUSerName_Click);
            // 
            // txtazure
            // 
            this.txtazure.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtazure.Location = new System.Drawing.Point(251, 121);
            this.txtazure.Name = "txtazure";
            this.txtazure.Size = new System.Drawing.Size(279, 22);
            this.txtazure.TabIndex = 50;
            this.txtazure.Visible = false;
            // 
            // lblazureserver
            // 
            this.lblazureserver.AutoSize = true;
            this.lblazureserver.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblazureserver.ForeColor = System.Drawing.Color.Black;
            this.lblazureserver.Location = new System.Drawing.Point(113, 120);
            this.lblazureserver.Name = "lblazureserver";
            this.lblazureserver.Size = new System.Drawing.Size(60, 16);
            this.lblazureserver.TabIndex = 49;
            this.lblazureserver.Text = "ServerUrl";
            // 
            // txtMTMPassword
            // 
            this.txtMTMPassword.Location = new System.Drawing.Point(252, 174);
            this.txtMTMPassword.Name = "txtMTMPassword";
            this.txtMTMPassword.PasswordChar = '*';
            this.txtMTMPassword.Size = new System.Drawing.Size(279, 22);
            this.txtMTMPassword.TabIndex = 48;
            this.txtMTMPassword.UseSystemPasswordChar = true;
            // 
            // txtMTMUserName
            // 
            this.txtMTMUserName.Font = new System.Drawing.Font("Century Gothic", 9F);
            this.txtMTMUserName.Location = new System.Drawing.Point(252, 148);
            this.txtMTMUserName.Name = "txtMTMUserName";
            this.txtMTMUserName.Size = new System.Drawing.Size(279, 22);
            this.txtMTMUserName.TabIndex = 47;
            this.txtMTMUserName.TextChanged += new System.EventHandler(this.txtMTMUserName_TextChanged);
            // 
            // lblMTMpassword
            // 
            this.lblMTMpassword.AutoSize = true;
            this.lblMTMpassword.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblMTMpassword.ForeColor = System.Drawing.Color.Black;
            this.lblMTMpassword.Location = new System.Drawing.Point(113, 174);
            this.lblMTMpassword.Name = "lblMTMpassword";
            this.lblMTMpassword.Size = new System.Drawing.Size(63, 16);
            this.lblMTMpassword.TabIndex = 46;
            this.lblMTMpassword.Text = "Password";
            // 
            // lblMTMUserName
            // 
            this.lblMTMUserName.AutoSize = true;
            this.lblMTMUserName.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblMTMUserName.ForeColor = System.Drawing.Color.Black;
            this.lblMTMUserName.Location = new System.Drawing.Point(113, 147);
            this.lblMTMUserName.Name = "lblMTMUserName";
            this.lblMTMUserName.Size = new System.Drawing.Size(69, 16);
            this.lblMTMUserName.TabIndex = 45;
            this.lblMTMUserName.Text = "UserName";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold);
            this.label11.Location = new System.Drawing.Point(271, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(315, 18);
            this.label11.TabIndex = 44;
            this.label11.Text = "TEST REPOSITORY CONFIGURATION\r\n";
            // 
            // txtqTest
            // 
            this.txtqTest.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqTest.Location = new System.Drawing.Point(251, 122);
            this.txtqTest.Name = "txtqTest";
            this.txtqTest.Size = new System.Drawing.Size(279, 22);
            this.txtqTest.TabIndex = 43;
            this.txtqTest.Visible = false;
            // 
            // lblqTestserver
            // 
            this.lblqTestserver.AutoSize = true;
            this.lblqTestserver.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblqTestserver.ForeColor = System.Drawing.Color.Black;
            this.lblqTestserver.Location = new System.Drawing.Point(111, 120);
            this.lblqTestserver.Name = "lblqTestserver";
            this.lblqTestserver.Size = new System.Drawing.Size(60, 16);
            this.lblqTestserver.TabIndex = 42;
            this.lblqTestserver.Text = "ServerUrl";
            // 
            // txtmtm
            // 
            this.txtmtm.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmtm.Location = new System.Drawing.Point(251, 121);
            this.txtmtm.Name = "txtmtm";
            this.txtmtm.Size = new System.Drawing.Size(279, 22);
            this.txtmtm.TabIndex = 41;
            this.txtmtm.Visible = false;
            // 
            // lblmtmserver
            // 
            this.lblmtmserver.AutoSize = true;
            this.lblmtmserver.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblmtmserver.ForeColor = System.Drawing.Color.Black;
            this.lblmtmserver.Location = new System.Drawing.Point(113, 119);
            this.lblmtmserver.Name = "lblmtmserver";
            this.lblmtmserver.Size = new System.Drawing.Size(60, 16);
            this.lblmtmserver.TabIndex = 32;
            this.lblmtmserver.Text = "ServerUrl";
            // 
            // cmbtestexecution
            // 
            this.cmbtestexecution.AutoCompleteCustomSource.AddRange(new string[] {
            "MTM",
            "qTest"});
            this.cmbtestexecution.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbtestexecution.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbtestexecution.FormattingEnabled = true;
            this.cmbtestexecution.Location = new System.Drawing.Point(252, 76);
            this.cmbtestexecution.Name = "cmbtestexecution";
            this.cmbtestexecution.Size = new System.Drawing.Size(279, 25);
            this.cmbtestexecution.TabIndex = 31;
            this.cmbtestexecution.SelectedIndexChanged += new System.EventHandler(this.cmbtestexecution_SelectedIndexChanged);
            // 
            // lbltestrepositoty
            // 
            this.lbltestrepositoty.AutoSize = true;
            this.lbltestrepositoty.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lbltestrepositoty.ForeColor = System.Drawing.Color.Black;
            this.lbltestrepositoty.Location = new System.Drawing.Point(113, 82);
            this.lbltestrepositoty.Name = "lbltestrepositoty";
            this.lbltestrepositoty.Size = new System.Drawing.Size(111, 16);
            this.lbltestrepositoty.TabIndex = 30;
            this.lbltestrepositoty.Text = "Repositiory Name";
            // 
            // tabTestResult
            // 
            this.tabTestResult.Controls.Add(this.panel4);
            this.tabTestResult.Location = new System.Drawing.Point(4, 22);
            this.tabTestResult.Name = "tabTestResult";
            this.tabTestResult.Size = new System.Drawing.Size(831, 410);
            this.tabTestResult.TabIndex = 5;
            this.tabTestResult.Text = "Test Execution Result";
            this.tabTestResult.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel4.BackgroundImage")));
            this.panel4.Controls.Add(this.btnRun);
            this.panel4.Controls.Add(this.dateTimePickerEnd);
            this.panel4.Controls.Add(this.label17);
            this.panel4.Controls.Add(this.dateTimePickerStart);
            this.panel4.Controls.Add(this.label16);
            this.panel4.Controls.Add(this.dataGridView1);
            this.panel4.Controls.Add(this.lblTestSatusGrid);
            this.panel4.Controls.Add(this.cmbboxtestresults);
            this.panel4.Controls.Add(this.lblestCaseId);
            this.panel4.Controls.Add(this.label14);
            this.panel4.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.panel4.Location = new System.Drawing.Point(-4, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(832, 372);
            this.panel4.TabIndex = 0;
            // 
            // btnRun
            // 
            this.btnRun.Font = new System.Drawing.Font("Verdana", 9.75F);
            this.btnRun.Image = global::IfestTool.Properties.Resources.submit;
            this.btnRun.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRun.Location = new System.Drawing.Point(562, 138);
            this.btnRun.Name = "btnRun";
            this.btnRun.Padding = new System.Windows.Forms.Padding(0, 3, 8, 4);
            this.btnRun.Size = new System.Drawing.Size(85, 30);
            this.btnRun.TabIndex = 41;
            this.btnRun.Text = "Submit";
            this.btnRun.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.BtnRun_Click);
            // 
            // dateTimePickerEnd
            // 
            this.dateTimePickerEnd.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerEnd.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerEnd.Location = new System.Drawing.Point(407, 138);
            this.dateTimePickerEnd.Name = "dateTimePickerEnd";
            this.dateTimePickerEnd.Size = new System.Drawing.Size(121, 22);
            this.dateTimePickerEnd.TabIndex = 40;
            this.dateTimePickerEnd.ValueChanged += new System.EventHandler(this.DateTimePickerEnd_ValueChanged);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.label17.ForeColor = System.Drawing.Color.Black;
            this.label17.Location = new System.Drawing.Point(313, 138);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(59, 16);
            this.label17.TabIndex = 39;
            this.label17.Text = "End Date";
            // 
            // dateTimePickerStart
            // 
            this.dateTimePickerStart.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePickerStart.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePickerStart.Location = new System.Drawing.Point(165, 133);
            this.dateTimePickerStart.Name = "dateTimePickerStart";
            this.dateTimePickerStart.Size = new System.Drawing.Size(121, 22);
            this.dateTimePickerStart.TabIndex = 38;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.label16.ForeColor = System.Drawing.Color.Black;
            this.label16.Location = new System.Drawing.Point(25, 132);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 16);
            this.label16.TabIndex = 37;
            this.label16.Text = "Start Date";
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(165, 185);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(460, 158);
            this.dataGridView1.TabIndex = 32;
            // 
            // lblTestSatusGrid
            // 
            this.lblTestSatusGrid.AutoSize = true;
            this.lblTestSatusGrid.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblTestSatusGrid.ForeColor = System.Drawing.Color.Black;
            this.lblTestSatusGrid.Location = new System.Drawing.Point(25, 175);
            this.lblTestSatusGrid.Name = "lblTestSatusGrid";
            this.lblTestSatusGrid.Size = new System.Drawing.Size(65, 16);
            this.lblTestSatusGrid.TabIndex = 31;
            this.lblTestSatusGrid.Text = "Test Status";
            // 
            // cmbboxtestresults
            // 
            this.cmbboxtestresults.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbboxtestresults.FormattingEnabled = true;
            this.cmbboxtestresults.Location = new System.Drawing.Point(165, 85);
            this.cmbboxtestresults.Name = "cmbboxtestresults";
            this.cmbboxtestresults.Size = new System.Drawing.Size(121, 25);
            this.cmbboxtestresults.TabIndex = 30;
            this.cmbboxtestresults.SelectedIndexChanged += new System.EventHandler(this.ComboBox1_SelectedIndexChanged);
            // 
            // lblestCaseId
            // 
            this.lblestCaseId.AutoSize = true;
            this.lblestCaseId.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold);
            this.lblestCaseId.ForeColor = System.Drawing.Color.Black;
            this.lblestCaseId.Location = new System.Drawing.Point(25, 85);
            this.lblestCaseId.Name = "lblestCaseId";
            this.lblestCaseId.Size = new System.Drawing.Size(80, 16);
            this.lblestCaseId.TabIndex = 29;
            this.lblestCaseId.Text = "TestCase Ids";
            this.lblestCaseId.Click += new System.EventHandler(this.LblestCaseId_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold);
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(323, 25);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(163, 18);
            this.label14.TabIndex = 25;
            this.label14.Text = "TEST RUN REPORT";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(21, 239);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(0, 15);
            this.label9.TabIndex = 19;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(15, 219);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 15);
            this.label8.TabIndex = 18;
            // 
            // pnlInterval
            // 
            this.pnlInterval.Controls.Add(this.label5);
            this.pnlInterval.Controls.Add(this.cmbInterval);
            this.pnlInterval.Controls.Add(this.label4);
            this.pnlInterval.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlInterval.ForeColor = System.Drawing.Color.FloralWhite;
            this.pnlInterval.Location = new System.Drawing.Point(6, 139);
            this.pnlInterval.Name = "pnlInterval";
            this.pnlInterval.Size = new System.Drawing.Size(301, 60);
            this.pnlInterval.TabIndex = 16;
            this.pnlInterval.Visible = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(209, 13);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(46, 15);
            this.label5.TabIndex = 8;
            this.label5.Text = "(hours)";
            // 
            // cmbInterval
            // 
            this.cmbInterval.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbInterval.FormattingEnabled = true;
            this.cmbInterval.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
            this.cmbInterval.Location = new System.Drawing.Point(98, 7);
            this.cmbInterval.Name = "cmbInterval";
            this.cmbInterval.Size = new System.Drawing.Size(99, 26);
            this.cmbInterval.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(9, 10);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 15);
            this.label4.TabIndex = 6;
            this.label4.Text = "Interval";
            // 
            // rdbDaily
            // 
            this.rdbDaily.AutoSize = true;
            this.rdbDaily.Checked = true;
            this.rdbDaily.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbDaily.ForeColor = System.Drawing.Color.Black;
            this.rdbDaily.Location = new System.Drawing.Point(113, 45);
            this.rdbDaily.Name = "rdbDaily";
            this.rdbDaily.Size = new System.Drawing.Size(52, 19);
            this.rdbDaily.TabIndex = 11;
            this.rdbDaily.TabStop = true;
            this.rdbDaily.Text = "Daily";
            this.rdbDaily.UseVisualStyleBackColor = true;
            // 
            // rdbHourly
            // 
            this.rdbHourly.AutoSize = true;
            this.rdbHourly.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbHourly.ForeColor = System.Drawing.Color.Black;
            this.rdbHourly.Location = new System.Drawing.Point(181, 45);
            this.rdbHourly.Name = "rdbHourly";
            this.rdbHourly.Size = new System.Drawing.Size(60, 19);
            this.rdbHourly.TabIndex = 12;
            this.rdbHourly.Text = "Hourly";
            this.rdbHourly.UseVisualStyleBackColor = true;
            // 
            // dtStartTime
            // 
            this.dtStartTime.CustomFormat = "HH:mm";
            this.dtStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtStartTime.Location = new System.Drawing.Point(107, 81);
            this.dtStartTime.Name = "dtStartTime";
            this.dtStartTime.ShowUpDown = true;
            this.dtStartTime.Size = new System.Drawing.Size(96, 20);
            this.dtStartTime.TabIndex = 15;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(15, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 15);
            this.label3.TabIndex = 14;
            this.label3.Text = "Start Time:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(109, 275);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(298, 20);
            this.textBox1.TabIndex = 27;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(12, 280);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 15);
            this.label7.TabIndex = 26;
            this.label7.Text = "Project Name";
            // 
            // txtChangeset
            // 
            this.txtChangeset.Enabled = false;
            this.txtChangeset.Location = new System.Drawing.Point(109, 165);
            this.txtChangeset.Name = "txtChangeset";
            this.txtChangeset.Size = new System.Drawing.Size(298, 20);
            this.txtChangeset.TabIndex = 25;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(13, 165);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(74, 15);
            this.label1.TabIndex = 22;
            this.label1.Text = "Commit ID";
            // 
            // txtSrvUrl
            // 
            this.txtSrvUrl.Location = new System.Drawing.Point(109, 202);
            this.txtSrvUrl.Name = "txtSrvUrl";
            this.txtSrvUrl.Size = new System.Drawing.Size(300, 20);
            this.txtSrvUrl.TabIndex = 23;
            // 
            // lblSrvUrl
            // 
            this.lblSrvUrl.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSrvUrl.ForeColor = System.Drawing.Color.Black;
            this.lblSrvUrl.Location = new System.Drawing.Point(13, 202);
            this.lblSrvUrl.Name = "lblSrvUrl";
            this.lblSrvUrl.Size = new System.Drawing.Size(52, 15);
            this.lblSrvUrl.TabIndex = 22;
            this.lblSrvUrl.Text = "Server";
            // 
            // txtSrvToken
            // 
            this.txtSrvToken.Location = new System.Drawing.Point(109, 238);
            this.txtSrvToken.Name = "txtSrvToken";
            this.txtSrvToken.Size = new System.Drawing.Size(298, 20);
            this.txtSrvToken.TabIndex = 21;
            // 
            // lblSrvToken
            // 
            this.lblSrvToken.AutoSize = true;
            this.lblSrvToken.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSrvToken.ForeColor = System.Drawing.Color.Black;
            this.lblSrvToken.Location = new System.Drawing.Point(13, 242);
            this.lblSrvToken.Name = "lblSrvToken";
            this.lblSrvToken.Size = new System.Drawing.Size(46, 15);
            this.lblSrvToken.TabIndex = 18;
            this.lblSrvToken.Text = "Token";
            // 
            // rbTFS
            // 
            this.rbTFS.AutoSize = true;
            this.rbTFS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbTFS.ForeColor = System.Drawing.Color.Black;
            this.rbTFS.Location = new System.Drawing.Point(128, 40);
            this.rbTFS.Name = "rbTFS";
            this.rbTFS.Size = new System.Drawing.Size(40, 19);
            this.rbTFS.TabIndex = 14;
            this.rbTFS.TabStop = true;
            this.rbTFS.Text = "Git";
            this.rbTFS.UseVisualStyleBackColor = true;
            // 
            // lblSC
            // 
            this.lblSC.AutoSize = true;
            this.lblSC.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSC.ForeColor = System.Drawing.Color.Black;
            this.lblSC.Location = new System.Drawing.Point(12, 43);
            this.lblSC.Name = "lblSC";
            this.lblSC.Size = new System.Drawing.Size(52, 15);
            this.lblSC.TabIndex = 13;
            this.lblSC.Text = "Source";
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(340, 322);
            this.panel1.TabIndex = 20;
            // 
            // frmDashBoard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(830, 448);
            this.Controls.Add(this.tabScheduler);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "frmDashBoard";
            this.Text = "Intelligently Filter Schedule and Execute Tests";
            this.Load += new System.EventHandler(this.frmDashBoard_Load);
            this.Resize += new System.EventHandler(this.frmDashBoard_Resize);
            this.cmsMenu.ResumeLayout(false);
            this.tabScheduler.ResumeLayout(false);
            this.tabConfigure.ResumeLayout(false);
            this.tabSchedule.ResumeLayout(false);
            this.pnlScheduler.ResumeLayout(false);
            this.pnlScheduler.PerformLayout();
            this.pnlSchedulerInterval.ResumeLayout(false);
            this.pnlSchedulerInterval.PerformLayout();
            this.tabCodeBase.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabExecReports.ResumeLayout(false);
            this.tabExecReports.PerformLayout();
            this.tabTestExec.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.tabTestResult.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.pnlInterval.ResumeLayout(false);
            this.pnlInterval.PerformLayout();
            this.ResumeLayout(false);

        }



        #endregion

        private System.Windows.Forms.ContextMenuStrip cmsMenu;
        private System.Windows.Forms.ToolStripMenuItem miSettings;
        private System.Windows.Forms.ToolStripMenuItem miConfiguration;
        private System.Windows.Forms.ToolStripMenuItem miClose;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.TabControl tabScheduler;
        private System.Windows.Forms.TabPage tabConfigure;
        private System.Windows.Forms.TabPage tabCodeBase;
        private System.Windows.Forms.Button btnCancelCnfg;
       private System.Windows.Forms.Button btnSaveSettings;
        private System.Windows.Forms.ListView lvTests;
        private System.Windows.Forms.ListView lvClasses;
        private System.Windows.Forms.ColumnHeader chCLassName;
        private System.Windows.Forms.ColumnHeader chTestName;
        private System.Windows.Forms.Button btnAddTest;
        private System.Windows.Forms.Button btnDeleteTest;
        private System.Windows.Forms.Button btnDeleteTestSymbol;
        private System.Windows.Forms.Button btnAddTestSymbol;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtChangeset;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtSrvUrl;
        private System.Windows.Forms.Label lblSrvUrl;
        private System.Windows.Forms.TextBox txtSrvToken;
        private System.Windows.Forms.Label lblSrvToken;
        private System.Windows.Forms.RadioButton rbTFS;
        private System.Windows.Forms.Label lblSC;
        private System.Windows.Forms.TabPage tabExecReports;
        private System.Windows.Forms.TreeView tvSchExecResults;
        private System.Windows.Forms.ToolStripMenuItem miReport;
        private System.Windows.Forms.ImageList imgListTV;
        private System.Windows.Forms.Label lblNoReps;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cmbSource;
        private TextBox txtCommitId;
        private TextBox txtServer;
        private Label lblServer;
        private Label lblCommitId;
        private TextBox txtToken;
        private Label lblToken;
        private TextBox txtProjectName;
        private Label lblProjectName;
        private TextBox txtgithubproduct;
        private TextBox txtgitauthenticationcode;
        private Label lblgitauthenticationcode;
        private TextBox txtgitclientsecret;
        private Label lblgitClientSecret;
        private Label lblgitClientId;
        private TextBox txtgitClientId;
        private TabPage tabTestExec;
        private Panel panel3;
        private ComboBox cmbtestexecution;
        private Label lbltestrepositoty;
        private TextBox txtqTest;
        private Label lblqTestserver;
        private TextBox txtmtm;
        private Label lblmtmserver;
        private Button btnexecute;
        private Label label11;
        private TabPage tabSchedule;
        private Panel pnlScheduler;
        private Label label12;
        private Panel pnlSchedulerInterval;
        private Label label13;
        private ComboBox cmbIntervals;
        private Label lblschedulerinterval;
        private RadioButton rdbschedulerdaily;
        private RadioButton rdbschedulerhourly;
        private DateTimePicker dateTimeescheduler;
        private Label lblschedulerStart;
        private Button btnSaveSchedulerSettings;
        private Button btnCancelSchedulerSettings;
        private Panel panel1;
        private Label label9;
        private Label label8;
        private Panel pnlInterval;
        private Label label5;
        private ComboBox cmbInterval;
        private Label label4;
        private RadioButton rdbDaily;
        private RadioButton rdbHourly;
        private DateTimePicker dtStartTime;
        private Label label3;
        private TextBox txtMTMPassword;
        private TextBox txtMTMUserName;
        private Label lblMTMpassword;
        private Label lblMTMUserName;
        private Button btnSave;
        private TabPage tabTestResult;
        private Panel panel4;
        private ComboBox cmbboxtestresults;
        private Label lblestCaseId;
        private Label label14;
        private DataGridView dataGridView1;
        private Label lblTestSatusGrid;
        private Button btnCancel;
        private Label label15;
        private DateTimePicker dateTimePickerEnd;
        private Label label17;
        private DateTimePicker dateTimePickerStart;
        private Label label16;
        private Button btnRun;
        private TextBox txtazure;
        private Label lblazureserver;
        private TextBox txtAzurePAT;
        private Label lblAzurePAT;
        private TextBox txtqtestUserName;
        private Label lblqTestUSerName;
        private TextBox txtqtestPassword;
        private Label lblqtestPassword;
        private Label lblMTMenvironment;
        private Label lblMTMconfigId;
        private Label lblMTMsettingsId;
        private Label lblMTMsuiteId;
        private Label lblMTMtestplanid;
        private TextBox txtAzuretestplanId;
        private Label lblAzuretestplanId;
        private TextBox txtMTMtestplanId;
        private TextBox txtAzuretestsuiteId;
        private TextBox txtMTMtestsuiteId;
        private Label lblAzuretestsuiteId;
        private TextBox txtAzureSettingsID;
        private Label lblAzureSettingsId;
        private TextBox txtMTMsettingsID;
        private TextBox txtAzureconfigId;
        private Label lblAzureconfigId;
        private TextBox txtMTMconfigID;
        private TextBox txtAzureEnvironment;
        private Label lblAzureEnvironment;
        private TextBox txtMTMenvironment;
    }
}

