﻿namespace IfestTool
{
    partial class TestCaseDialogForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvTestCases = new System.Windows.Forms.DataGridView();
            this.btntestcaseCompletion = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTestCases)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvTestCases
            // 
            this.dgvTestCases.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvTestCases.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTestCases.Location = new System.Drawing.Point(38, 24);
            this.dgvTestCases.Name = "dgvTestCases";
            this.dgvTestCases.Size = new System.Drawing.Size(424, 262);
            this.dgvTestCases.TabIndex = 0;
            this.dgvTestCases.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvTestCases_CellClick);
            this.dgvTestCases.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DgvTestCases_CellContentClick);
            // 
            // btntestcaseCompletion
            // 
            this.btntestcaseCompletion.Location = new System.Drawing.Point(252, 324);
            this.btntestcaseCompletion.Name = "btntestcaseCompletion";
            this.btntestcaseCompletion.Size = new System.Drawing.Size(154, 47);
            this.btntestcaseCompletion.TabIndex = 1;
            this.btntestcaseCompletion.Text = "Add Tests";
            this.btntestcaseCompletion.UseVisualStyleBackColor = true;
            this.btntestcaseCompletion.Click += new System.EventHandler(this.BtntestcaseCompletion_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(90, 324);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(125, 47);
            this.button1.TabIndex = 2;
            this.button1.Text = "Cancel Selection";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // TestCaseDialogForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BackgroundImage = global::IfestTool.Properties.Resources.SchedulerImg;
            this.ClientSize = new System.Drawing.Size(600, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btntestcaseCompletion);
            this.Controls.Add(this.dgvTestCases);
            this.Name = "TestCaseDialogForm";
            this.Text = "TestCaseSelectionForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.testCaseDialogForm_FormClosing);
            //this.Load += new System.EventHandler(this.TestCaseDialogForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTestCases)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvTestCases;
        private System.Windows.Forms.Button btntestcaseCompletion;
        private System.Windows.Forms.Button button1;
    }
}