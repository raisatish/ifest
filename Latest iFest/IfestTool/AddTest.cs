﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace IfestTool
{
    public partial class frmAddTest : Form
    {
        public frmAddTest()
        {
            InitializeComponent();
        }

        public string TestName
        {
            get;
            private set;
        }

        private void btnSaveTest_Click(object sender, EventArgs e)
        {
            if (!IsValidTest())
            {
                MessageBox.Show("Please provide Test Name.", "Error !");
                this.DialogResult = DialogResult.None;
            }

            TestName = txtTestName.Text;
        }


        private bool IsValidTest()
        {
            bool isValid = false;

            isValid = txtTestName.Text.Trim().Length > 0;

            return isValid;
        }

        private void btnCancelTest_Click(object sender, EventArgs e)
        {
            ClearData();

            this.Close();
        }

        private void AddTest_Load(object sender, EventArgs e)
        {
            this.ClearData();
        }

        private void ClearData()
        {
            txtTestName.Text = string.Empty;
           
            this.TestName = string.Empty;
        }

        private void frmAddTest_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.None)
            {
                e.Cancel = true;
            }
        }
    }
}
