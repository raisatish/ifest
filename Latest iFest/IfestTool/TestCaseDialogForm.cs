﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.TeamFoundation.Test.WebApi;
using Microsoft.TeamFoundation.TestManagement.Client;
using Models;
namespace IfestTool
{
    
    public partial class TestCaseDialogForm : Form
    {
        TestCaseDialog testCaseDialog = new TestCaseDialog();
        frmDashBoard frm = new frmDashBoard();
        public List<string> testCaseIds = new List<string>();
        public TestCaseDialogForm()
        {
            InitializeComponent();
            
            dgvTestCases.DataSource = frm.GetAzuredevOpsTestCases();

            LoadTestCases();
        }                  
        private void LoadTestCases()
        {           
            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Name = "isSelected";
            checkColumn.HeaderText = "isSelected";
            dgvTestCases.Columns.Add(checkColumn);
            dgvTestCases.Columns[0].Width = 70;
            dgvTestCases.Columns[1].Width = 200;
            dgvTestCases.Columns[2].Width = 60;
        }

        private void BtntestcaseCompletion_Click(object sender, EventArgs e)
        {

            if (!IsValidTest())
            {
               //MessageBox.Show("Please provide Test Name.", "Error !");
                this.DialogResult = DialogResult.None;
            }

            this.DialogResult = DialogResult.OK;
            frm.DialogResult = DialogResult.OK;
            //this.Close();
            //testCaseIds.ForEach(tId => frm.AddTestCases(tId));

            //frm.ShowTestCases();


        }

        private bool IsValidTest()
        {
            bool isValid = false;

            isValid = testCaseIds.Count > 0;

            return isValid;
        }
       
        private void DgvTestCases_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void DgvTestCases_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (Convert.ToBoolean(dgvTestCases.Rows[e.RowIndex].Cells[0].EditedFormattedValue) == true)
            {
                testCaseIds.Remove("TC" + dgvTestCases.Rows[e.RowIndex].Cells[1].Value);
                //frm.DeleteTestCases("TC" + dgvTestCases.Rows[e.RowIndex].Cells[1].Value, dgvTestCases.Rows[e.RowIndex].Cells[2].Value.ToString());
            }
            else
            {
                testCaseIds.Add("TC" + dgvTestCases.Rows[e.RowIndex].Cells[1].Value);
                //frm.AddTestCases("TC"+ dgvTestCases.Rows[e.RowIndex].Cells[1].Value, dgvTestCases.Rows[e.RowIndex].Cells[2].Value.ToString());
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void testCaseDialogForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult == DialogResult.None)
            {
                e.Cancel = true;
            }
        }       
    }
}
