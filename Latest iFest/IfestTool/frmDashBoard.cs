﻿using Injections;
using Interfaces;
using Models;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using System.IO;
using System.Configuration;
using System.Drawing;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.TeamFoundation;
using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.TestManagement.Client;
using System.Net;
using Microsoft.TeamFoundation.WorkItemTracking.Client;
using Microsoft.VisualStudio.Services.Common;
using Microsoft.TeamFoundation.Server;
using Microsoft.TeamFoundation.Test.WebApi;
using Microsoft.VisualStudio.Services.WebApi;
using Microsoft.TeamFoundation.SourceControl.WebApi;
using Microsoft.TeamFoundation.WorkItemTracking.WebApi;
using Microsoft.TeamFoundation.WorkItemTracking.WebApi.Models;
using System.Data;
using Microsoft.TeamFoundation.Framework.Client;

namespace IfestTool
{
    public partial class frmDashBoard : BaseForm
    {
        private IRepository<SchedulerSettings> settingsRepository { get; set; }
        private IRepository<SClass> classRepository { get; set; }
        private IRepository<STest> testRepository { get; set; }
        private IRepository<SchExecResult> schResRepository { get; set; }
        private IRepository<TfsConnectionDetails> tfsSettingsRepository { get; set; }
        private IRepository<AzureDevOpsConnectionDetails> azureSettingsRepository { get; set; }
        private ICommClient Client { get; set; }
        private SchedulerSettings schedulerSettings;
        private TfsConnectionDetails tfsConnectionDetails;
        private AzureDevOpsConnectionDetails azureDevOpsConnectionDetails;
        private TestsConfig testConfig;
        public static string gitCommitID;
        private Panel githubpanel = new Panel();
        private Panel tfspanel = new Panel();
        private static int selClsIndex {get; set;}
        private static string selectedId { get; set; }
       
        public frmDashBoard()
        {
            tfspanel.Name = "TFSGitPanel";
            tfspanel.Location = new System.Drawing.Point(100, 96);
            tfspanel.Size = new System.Drawing.Size(251, 122);
            githubpanel.Name = "GitHubPanel";
            githubpanel.Location = new System.Drawing.Point(100, 96);
            githubpanel.Size = new System.Drawing.Size(251, 122);

            InitializeComponent();

            MessageLogger.Log("Application Started...");
            settingsRepository = Injector.GetObject<IRepository<SchedulerSettings>>();
            classRepository = Injector.GetObject<IRepository<SClass>>();
            testRepository = Injector.GetObject<IRepository<STest>>();
            schResRepository = Injector.GetObject<IRepository<SchExecResult>>();
            tfsSettingsRepository = Injector.GetObject<IRepository<TfsConnectionDetails>>();
            azureSettingsRepository = Injector.GetObject<IRepository<AzureDevOpsConnectionDetails>>();

            var azureSettingsRepositoryItems = azureSettingsRepository.Get(string.Empty);

            var settingRepositoryItems = settingsRepository.Get(string.Empty);
            cmbSource.Items.AddRange(settingRepositoryItems.SourceControlType.Split(',').ToArray<object>());

            var tfsSettingsRepositoryItems = tfsSettingsRepository.Get(string.Empty);
            cmbtestexecution.Items.AddRange(tfsSettingsRepositoryItems.TestServerType.Split(',').ToArray<object>());
            cmbtestexecution.SelectedIndex = 1;
            LoadTestCaseIds();
            schedulerSettings = null;
            new Thread(() =>
            {
                Thread.Sleep(1800);
                Client = Injector.GetObject<ICommClient>();
                Client.GotDataFromServer += Client_GotDataFromServer;
                Client.Connect("127.0.0.8", 369);
            }).Start();
        }

        public void SendDataToServer(string Command, object CommandData)
        {
            var cmdText = JsonConvert.SerializeObject(new { Command = Command, CommandData = CommandData });
            Client.Send(cmdText);
        }

        private void Client_GotDataFromServer(object sender, string msg)
        {
            try
            {
                dynamic mo = JsonConvert.DeserializeObject(msg);

                try
                {
                    string CommandName = mo.Command;

                    if (CommandName == Commands.ServerSettings)
                    {
                        if (schedulerSettings == null)
                            schedulerSettings = settingsRepository.Get(string.Empty);
                        gitCommitID = schedulerSettings.gitPriviousCommitID;
                        SendDataToServer(CommandName, schedulerSettings);

                    }
                    else if (CommandName == Commands.ModifiedClassesList)
                    {
                        var csvModifiedClasses = mo.CommandData.ToString();

                        RunTestsForClasses(csvModifiedClasses);
                    }
                    else if (CommandName == Commands.UpdateChangeset)
                    {
                        schedulerSettings.ChangesetNo = mo.CommandData.ToString();
                        settingsRepository.Update(string.Empty, schedulerSettings);
                        txtChangeset.Text = schedulerSettings.ChangesetNo;
                    }

                    return;
                }
                catch (Microsoft.CSharp.RuntimeBinder.RuntimeBinderException be)
                {
                    MessageLogger.Log("Seems that invalid command received - " + be.Message);
                }
            }
            catch (Exception ex)
            {
                MessageLogger.Log(ex);
            }
        }

        private void RunTestsForClasses(string csvModifiedClasses)
        {
            string[] arrModifiedClasses;
            List<SClass> clsAllClassess;
            List<string> lstTestsToRun = new List<string>();
            string csvTestsToRun = string.Empty;

            try
            {
                // var testsToRunPath = System.IO.Path.Combine(Environment.CurrentDirectory, "..\\..\\..\\IFestDemoTest\\bin\\Debug\\TestsToRun.txt");
                //   var testsToRunPath = @"D:\TFSWorkSpace_Tool\Main\iFest\IFestDemoTest\bin\Debug\TestsToRun.txt";
                var testsToRunPath = ConfigurationManager.AppSettings.Get("testsToRunPath");
                if (File.Exists(testsToRunPath))
                {
                    File.Delete(testsToRunPath);
                }

                SchExecResult res = new SchExecResult();
                List<string> clsTests = new List<string>();
                System.Threading.Thread.Sleep(100);
                res.ExecDate = DateTime.Now;
                res.ModifiedClasses = new List<DiffClass>();

                //Get Tests associated with the modified classes and save them as Comma Seperate string in the specified file
                arrModifiedClasses = csvModifiedClasses.Replace("$", "").Split(',');

                if (arrModifiedClasses != null && arrModifiedClasses.Length > 0)
                {
                    clsAllClassess = classRepository.All();
                    foreach (string cls in arrModifiedClasses)
                    {
                        //var clsModified = clsAllClassess.FirstOrDefault(c => c.Name.Trim().Equals(cls.Trim(), StringComparison.InvariantCultureIgnoreCase));
                        var clsModified = clsAllClassess.FirstOrDefault(c => c.Name.Trim().Equals(cls.Trim(), StringComparison.InvariantCultureIgnoreCase));
                        bool clsDiff = clsAllClassess.Exists(x => string.Equals(x.Name.Trim(), cls.Trim(), StringComparison.OrdinalIgnoreCase));

                        clsTests = new List<string>();

                        if (clsModified != null)
                        {
                            foreach (STest tst in clsModified.Tests)
                            {
                                lstTestsToRun.Add(tst.Name);
                                clsTests.Add(tst.Name);
                            }

                            res.ModifiedClasses.Add(new DiffClass { ClassName = clsModified.Name, Tests = String.Join(",", clsTests) });
                        }

                        if (!clsDiff)
                        {
                            res.ModifiedClasses.Add(new DiffClass { ClassName = cls });
                        }
                    }

                    csvTestsToRun = String.Join(",", lstTestsToRun.Distinct());
                }

                schResRepository.Save(res);

                System.IO.File.WriteAllText(testsToRunPath, csvTestsToRun);

                Process myProcess = new Process();
                ProcessStartInfo myProcessStartInfo = new ProcessStartInfo("TestRunner.bat");
                //ProcessStartInfo myProcessStartInfo = new ProcessStartInfo("\"C:\\Program Files(x86)\\Microsoft Visual Studio\\2017\\Community\\Common7\\IDE\\MsTest.exe\"");

                myProcessStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                myProcessStartInfo.UseShellExecute = true;
                myProcess.StartInfo = myProcessStartInfo;
                myProcess.Start();

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        private void SendCommandToServer()
        {

        }

        private void miScheduler_Click(object sender, EventArgs e)
        {
            this.Show();
            tabScheduler.SelectedTab = tabCodeBase;
            this.tabCodeBase.Show();

            this.WindowState = FormWindowState.Normal;

            notifyIcon.Visible = false;
        }

        private void miConfiguration_Click(object sender, EventArgs e)
        {
            this.Show();
            tabScheduler.SelectedTab = tabConfigure;
            this.tabConfigure.Show();

            this.WindowState = FormWindowState.Normal;

            notifyIcon.Visible = false;
        }

        private void miReport_Click(object sender, EventArgs e)
        {
            this.Show();
            tabScheduler.SelectedTab = tabExecReports;
            this.tabExecReports.Show();

            this.WindowState = FormWindowState.Normal;

            notifyIcon.Visible = false;
        }

        private void miClose_Click(object sender, EventArgs e)
        {
            notifyIcon.Dispose();
            Application.Exit();
        }

        private void frmDashBoard_Resize(object sender, EventArgs e)
        {
            //if the form is minimized
            //hide it from the task bar
            //and show the system tray icon (represented by the NotifyIcon control)
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.Hide();
                notifyIcon.Visible = true;
                notifyIcon.ShowBalloonTip(500);
            }
        }

        private void tcTab_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (tabScheduler.SelectedTab == tabConfigure)
            {
                showTestsConfiguration();
            }
            else if (tabScheduler.SelectedTab == tabCodeBase)
            {
                showCodeBaseInfo();
            }
            else if (tabScheduler.SelectedTab == tabSchedule)
            {
                showSchedulerInfo();
            }
            else if (tabScheduler.SelectedTab == tabExecReports)
            {
                showSchedulerLogs();
            }

            else if (tabScheduler.SelectedTab == tabTestResult)
            {
                LoadTestCaseIds();
            }
        }

        private void LoadTestCaseIds()
        {
            List<string> fullList = new List<string>();
            var clsAllClassess = classRepository.All();
            clsAllClassess.ForEach(classObj => classObj.Tests.ForEach(test =>
            {

                fullList.Add(test.Name);
                if (!cmbboxtestresults.Items.Contains(test.Name))
                {
                    cmbboxtestresults.Items.Add(test.Name);
                }
            }
            ));

            var comboList = cmbboxtestresults.Items.OfType<string>().ToList();
            comboList.ForEach(item =>
            {
                if (!fullList.Any(b => b == item)) { cmbboxtestresults.Items.Remove(item); }
            }
            );

            if (!cmbboxtestresults.Items.Contains("All"))
            {
                cmbboxtestresults.Items.Add("All");
            }

            cmbboxtestresults.SelectedIndex = 0;
        }
        private void frmDashBoard_Load(object sender, EventArgs e)
        {
            tcTab_SelectedIndexChanged(tabScheduler, null);

            chCLassName.Width = lvClasses.Size.Width - 4;
            chTestName.Width = lvTests.Size.Width - 4;
            cmbSource.SelectedIndex = 0;
            cmbtestexecution.SelectedIndex = 1;
        }


        #region Scheduler Config related
        private void rdbDaily_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton rdbDaily = (RadioButton)sender;

            if (rdbDaily.Checked)
            {
                pnlInterval.Hide();
            }
            else
            {
                if (cmbInterval.SelectedIndex < 0)
                {
                    cmbInterval.SelectedIndex = 1;
                }

                pnlInterval.Show();
            }

        }

        private void showCodeBaseInfo()
        {
            try
            {
                schedulerSettings = settingsRepository.Get(string.Empty);
                SetCodeBasSettings(schedulerSettings);
            }
            catch (Exception ex)
            {
                MessageLogger.Log(ex);
                MessageBox.Show("Error while loading the Scheduler Configuration.", "Error !");
            }
        }

        private void showSchedulerInfo()
        {
            try
            {
                schedulerSettings = settingsRepository.Get(string.Empty);
                SetSchedulerSettings(schedulerSettings);
            }
            catch (Exception ex)
            {
                MessageLogger.Log(ex);
                MessageBox.Show("Error while loading the Scheduler Configuration.", "Error !");
            }
        }

        private void btnCancelCnfg_Click(object sender, EventArgs e)
        {
            if (schedulerSettings == null)
            {
                schedulerSettings = new SchedulerSettings();
                schedulerSettings.ServerUserName = string.Empty;
                schedulerSettings.ServerPassword = string.Empty;
                schedulerSettings.ServerAccessToken = string.Empty;
                schedulerSettings.ChangesetNo = string.Empty;
                schedulerSettings.ProjectName = string.Empty;
            }
            else
            {
                SetCodeBasSettings(schedulerSettings);
            }
        }

        
        private void SetSchedulerSettings(SchedulerSettings settings)
        {
            dtStartTime.Value = DateTime.ParseExact(settings.StartTime, "HH:mm", CultureInfo.InvariantCulture);

            if (settings.Frequency.Equals("Hourly"))
            {
                rdbschedulerhourly.Checked = true;
                cmbInterval.SelectedItem = settings.Interval;
                cmbInterval.SelectedIndex = settings.Interval.Value - 1;
                pnlInterval.Show();
                dateTimeescheduler.Text = settings.StartTime;
            }
            else
            {
                rdbschedulerdaily.Checked = true;
                dateTimeescheduler.Text = settings.StartTime;
                pnlInterval.Hide();
            }

        }

        private void SetCodeBasSettings(SchedulerSettings settings)
        {

            txtServer.Text = settings.ServerUrl;
            txtToken.Text = settings.ServerAccessToken;
            txtCommitId.Text = settings.ChangesetNo.ToString();
            txtProjectName.Text = settings.ProjectName.ToString();
        }

        private void btnSaveSettings_Click(object sender, EventArgs e)
        {
            SchedulerSettings newSettings = new SchedulerSettings();
            newSettings.ServerUrl = txtServer.Text;
            newSettings.ServerUserName = // txtSrvUsrName.Text;
            newSettings.ServerPassword = // txtSrvPwd.Text;
            newSettings.ServerAccessToken = txtToken.Text;
            newSettings.ChangesetNo = txtCommitId.Text;
            newSettings.ProjectName = txtProjectName.Text;
            newSettings.SourceControlType = cmbSource.SelectedItem.ToString();

            try
            {
                settingsRepository.Update(string.Empty, newSettings);
                schedulerSettings = newSettings;
                MessageBox.Show("Configuration has been updated successfully !", "Success !");
            }
            catch (Exception ex)
            {
                MessageLogger.Log(ex);
                MessageBox.Show("Error while updating the Scheduler Configuration.", "Error !");
            }

            SendDataToServer(Commands.ServerSettings, schedulerSettings);
        }
        #endregion


        #region Tests Config related
        private void showTestsConfiguration()
        {
            lvClasses.Focus();

           testConfig = new TestsConfig();
            try
            {
                testConfig.Classes = classRepository.All();
                ShowTfsClasses();
            }
            catch (Exception ex)
            {
                MessageLogger.Log(ex);
                MessageBox.Show("Error while loading Tests Configuration data", "Error !");
            }
        }

        //To pull all the files type(.cs) from TFS/TFSGit repositories
        private void ShowTfsClasses()
        {

            btnAddTest.Enabled = false;
            btnDeleteTest.Enabled = false;
            lvClasses.Items.Clear();
            schedulerSettings = settingsRepository.Get(string.Empty);
            using (HttpClient client = new HttpClient())
            {
                //Read acess token & URL from SchedulerConfig file 
                string AccessToken = schedulerSettings.ServerAccessToken;
                string Url = schedulerSettings.ServerUrl;



                client.BaseAddress = new Uri(Url);
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(
                                      System.Text.ASCIIEncoding.ASCII.GetBytes($":{AccessToken}")));



                using (HttpResponseMessage response = client.GetAsync("_apis/git/repositories/").Result)
                {
                    string responseBody = response.Content.ReadAsStringAsync().Result;
                    var responseBodyDetails = JsonConvert.DeserializeObject<TFSGitClassResposnse>(responseBody);

                    //To get proj id of TFS/Git repository
                    string projId = responseBodyDetails.value.Select(proj => proj.id).ToList()[0];
                    var classResponseFull = client.GetAsync("_apis/git/repositories/" + projId + "/items?recursionLevel=Full").Result;
                    string classResponseResult = classResponseFull.Content.ReadAsStringAsync().Result;



                    var wiDetails = JsonConvert.DeserializeObject<TfsGitFileListResponse>(classResponseResult);
                    var classNames = wiDetails.value.Select(c => c.path).Where(p => p.EndsWith(".cs") && !p.Contains(schedulerSettings.FilestoExclude)).ToList<string>();



                    classNames.Where(p => !p.Contains(schedulerSettings.FilestoExclude)).ToList().ForEach(cls =>
                    {
                        classRepository.Save(new SClass { Name = cls });
                    });
                }
            }



            testConfig.Classes = classRepository.All();
            //Mark of color(green,yellow) depending on the class having test cases to execute
            foreach (SClass a in testConfig.Classes)
            {
                ListViewItem lvItem = new ListViewItem { Text = a.Name, Tag = a.Id };
                if (a.Tests.Count > 0) lvItem.BackColor = ColorTranslator.FromHtml(schedulerSettings.ColorCombination.Split(',')[0]); else lvItem.BackColor = ColorTranslator.FromHtml(schedulerSettings.ColorCombination.Split(',')[1]);
                lvClasses.Items.Add(lvItem);
            }



            if (lvClasses.Items != null && lvClasses.Items.Count > 0)
            {
                //btnDeleteClass.Enabled = true;
                btnAddTest.Enabled = true;
                btnDeleteTest.Enabled = true;
                lvClasses.Items[0].Selected = true;
                lvClasses.Items[0].Focused = true;
            }
        }


        private void showTests(Guid classId)
        {
            btnDeleteTest.Enabled = false;
            testConfig.Classes = classRepository.All();
            List<STest> tests = testConfig.Classes.Find(a => a.Id == classId).Tests;
            lvTests.Refresh();

            lvTests.Items.Clear();

            foreach (STest t in tests)
            {
                this.lvTests.Items.Add(new ListViewItem { Text = t.Name, Tag = t.Id });
            }

            
            if (lvTests.Items != null && lvTests.Items.Count > 0)
            {
                lvTests.Items[0].Selected = true;
                lvTests.Items[0].Focused = true;

                btnDeleteTest.Enabled = true;
            }
            lvTests.Refresh();
            lvTests.Show();

        }
       
        private void btnAddClass_Click(object sender, EventArgs e)
        {
            using (frmAddClass dlgAddClass = new frmAddClass())
            {
                DialogResult dialogresult = dlgAddClass.ShowDialog(this);

                if (dialogresult == DialogResult.OK)
                {
                    try
                    {
                        SClass clsNew = classRepository.Save(new SClass { Name = dlgAddClass.ClassName });

                        if (clsNew == null)
                        {
                            MessageBox.Show("Class '" + dlgAddClass.ClassName + "' already exists. Please select a different name.", "Error !");
                        }
                        else
                        {
                            MessageBox.Show(dlgAddClass.ClassName + " has been added successfully..", "Success !");
                            showTestsConfiguration();
                            lvClasses.Items[lvClasses.Items.Count - 1].Selected = true;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageLogger.Log(ex);
                        MessageBox.Show("Error while adding the Class", "Error !");
                    }
                }
            }
        }

        private void lvClasses_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {           

            Guid classId = new Guid(Convert.ToString(e.Item.Tag));

            showTests(classId);
        }     
        public void AddTestCases(string testId, string strId)
        {
            try
            {
                testConfig = new TestsConfig();

                var testClass  = classRepository.Get(strId);

                if (!testClass.Tests.Any(x => x.Name == testId))
                    {
                    //List<STest> tests = testConfig.Classes.Find(a => a.Id == strId).Tests;

                    SClass cls = new SClass { Id = new Guid(strId), Tests = new List<STest>() };

                    cls.Tests.Add(new STest { Name = testId });

                    classRepository.Update(selectedId, cls);

                    MessageBox.Show(testId + " has been added successfully..", "Success !");
                }
            }

            catch (Exception ex)
            { }
        }
        public void DeleteTestCases(string strId, string testName)
        {
            testRepository.Delete(strId);
            MessageBox.Show("Test has been deleted successfully..", "Success !");
            showTestsConfiguration();
        }

        private void btnAddTest_Click(object sender, EventArgs e)
        {            
            selClsIndex = lvClasses.SelectedIndices[0];
            selectedId = lvClasses.SelectedItems[0].Tag.ToString();


            using (TestCaseDialogForm dlgAddTest = new TestCaseDialogForm())
            {
                DialogResult dialogresult = dlgAddTest.ShowDialog(this);

                if (dialogresult == DialogResult.OK)
                {
                    try
                    {
                        string strId = lvClasses.SelectedItems[0].Tag.ToString();
                        dlgAddTest.testCaseIds.ForEach(tId => AddTestCases(tId, strId));                        
                        showTestsConfiguration();
                        lvClasses.Items[selClsIndex].Selected = true;
                        lvTests.Items[lvTests.Items.Count - 1].Selected = true;
                    }
                    catch (Exception ex)
                    {
                        MessageLogger.Log(ex);
                        MessageBox.Show("Error while adding the Test", "Error !");
                    }
                }
            }           
        }

        private void btnDeleteTest_Click(object sender, EventArgs e)
        {
            string strTestId = lvTests.SelectedItems[0].Tag.ToString();
            int intSelClsIndex = lvClasses.SelectedIndices[0];

            try
            {
                testRepository.Delete(strTestId);
                MessageBox.Show("Test has been deleted successfully..", "Success !");
                showTestsConfiguration();
                lvClasses.Items[intSelClsIndex].Selected = true;
                if (lvTests.Items.Count > 0)
                {
                    lvTests.Items[lvTests.Items.Count - 1].Selected = true;
                }
            }
            catch (Exception ex)
            {
                MessageLogger.Log(ex);
                MessageBox.Show("Error while deleting the Test", "Error !");
            }
        }

        private void btnDeleteClass_Click(object sender, EventArgs e)
        {
            int intSelClsIndex = lvClasses.SelectedIndices[0];
            Guid classId = new Guid(lvClasses.SelectedItems[0].Tag.ToString());

            List<STest> tests = testConfig.Classes.Find(a => a.Id == classId).Tests;

            if (tests.Count > 0)
            {
                MessageBox.Show("Tests are associated with this Class. Please delete the tests first.", "Error !");
                return;
            }

            try
            {
                classRepository.Delete(classId.ToString());
                MessageBox.Show("Class has been deleted successfully..", "Success !");
                showTestsConfiguration();

                if (lvClasses.Items.Count > 0 && (intSelClsIndex >= lvClasses.Items.Count - 1))
                {
                    intSelClsIndex = lvClasses.Items.Count - 1;
                }

                if (lvClasses.Items.Count > 0)
                {
                    lvClasses.Items[intSelClsIndex].Selected = true;
                }
            }
            catch (Exception ex)
            {
                MessageLogger.Log(ex);
                MessageBox.Show("Error while deleting the Class", "Error !");
            }
        }
        #endregion

        #region Scheduler Log related
        private void showSchedulerLogs()
        {
            List<SchExecResult> res = null;

            try
            {
                res = schResRepository.All();
            }
            catch (Exception ex)
            {
                MessageLogger.Log(ex);
                MessageBox.Show("Error while loading Scheduler Execution Reports", "Error !");
            }

            BuildTreeView(res);
        }

        private void BuildTreeView(List<SchExecResult> results)
        {
            TreeNode tNode, clsNode;

            tvSchExecResults.Nodes.Clear();

            lblNoReps.Visible = true;
            tvSchExecResults.Visible = false;

            if (results != null)
            {
                lblNoReps.Visible = false;
                tvSchExecResults.Visible = true;

                foreach (SchExecResult r in results)
                {
                    tNode = new TreeNode();

                    tNode.Text = r.ExecDate.ToString("dddd, dd MMMM yyyy hh:mm tt");
                    tNode.ImageIndex = 1;
                    tNode.SelectedImageIndex = 1;


                    foreach (DiffClass c in r.ModifiedClasses)
                    {
                        clsNode = new TreeNode(c.ClassName);
                        clsNode.ImageIndex = 0;
                        clsNode.SelectedImageIndex = 0;

                        if (!c.Tests.Equals(string.Empty))
                        {
                            clsNode.Nodes.Add(new TreeNode(c.Tests, 2, 2));
                        }
                        else
                        {
                            clsNode.BackColor = System.Drawing.Color.Yellow;
                        }

                        tNode.Nodes.Add(clsNode);
                    }

                    tvSchExecResults.Nodes.Add(tNode);
                }

                tvSchExecResults.CollapseAll();
            }
        }




        #endregion

        private void cmbSource_SelectedIndexChanged(object sender, EventArgs e)
        {
            //128, 79
            switch (cmbSource.SelectedItem.ToString())
            {
                case "TFS":
                case "Azure DevOps":
                    lblCommitId.Visible = true;
                    txtCommitId.Visible = true;

                    lblServer.Visible = true;
                    txtServer.Visible = true;

                    lblToken.Visible = true;
                    txtToken.Visible = true;

                    lblProjectName.Visible = true;
                    txtProjectName.Visible = true;


                   // lblgithubproduct.Visible = false;
                  //  txtgithubproduct.Visible = false;

                   lblgitClientId.Visible = false;
                   txtgitClientId.Visible = false;

                    lblgitClientSecret.Visible = false;
                    txtgitclientsecret.Visible = false;

                    lblgitauthenticationcode.Visible = false;
                    txtgitauthenticationcode.Visible = false;
                    break;


                case "GitHub":
                    lblCommitId.Visible = false;
                    txtCommitId.Visible = false;

                    lblServer.Visible = false;
                    txtServer.Visible = false;

                    lblToken.Visible = false;
                    txtToken.Visible = false;

                    lblProjectName.Visible = false;
                    txtProjectName.Visible = false;

                   // lblgithubproduct.Visible = true;
                    txtgithubproduct.Visible = true;

                    lblgitClientId.Visible = true;
                    txtgitClientId.Visible = true;

                    lblgitClientSecret.Visible = true;
                    txtgitclientsecret.Visible = true;

                    lblgitauthenticationcode.Visible = true;
                    txtgitauthenticationcode.Visible = true;

                    lblgitauthenticationcode.Visible = true;
                    txtgitauthenticationcode.Visible = true;
                    break;
            }

        }       

        private void cmbtestexecution_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cmbtestexecution.SelectedItem.ToString())
            {
                case "MTM":
                    lblmtmserver.Visible = true;
                    txtmtm.Visible = true;

                    lblMTMUserName.Visible = true;
                    txtMTMUserName.Visible = true;

                    lblMTMpassword.Visible = true;
                    txtMTMPassword.Visible = true;

                    lblMTMtestplanid.Visible = true;
                    txtMTMtestplanId.Visible = true;

                    lblMTMsuiteId.Visible = true;
                    txtMTMtestsuiteId.Visible = true;

                    lblMTMconfigId.Visible = true;
                    txtMTMconfigID.Visible = true;

                    lblMTMenvironment.Visible = true;
                    txtMTMenvironment.Visible = true;


                    lblAzureEnvironment.Visible = false;
                    txtAzureEnvironment.Visible = false;

                    lblAzureconfigId.Visible = false;
                    txtAzureconfigId.Visible = false;

                    lblMTMsettingsId.Visible = true;
                    txtMTMsettingsID.Visible = true;

                    lblAzureSettingsId.Visible = false;
                    txtAzureSettingsID.Visible = false;

                    lblAzuretestsuiteId.Visible = false;
                    txtAzuretestsuiteId.Visible = false;

                    lblAzuretestplanId.Visible = false;
                    txtAzuretestplanId.Visible = false;



                    lblqtestPassword.Visible = false;
                    txtqtestPassword.Visible = false;
                

                    lblqTestserver.Visible = false;
                    txtqTest.Visible = false;

                    lblazureserver.Visible = false;
                    txtazure.Visible = false;

                    lblqTestUSerName.Visible = false;
                    txtqtestUserName.Visible = false;

                    lblAzurePAT.Visible = false;
                    txtAzurePAT.Visible = false;

                    SetTFSConncetionDetaiSettings(tfsConnectionDetails);
                    break;

                case "AzureDevOps":
                    lblazureserver.Visible = true;
                    txtazure.Visible = true;

                    lblAzurePAT.Visible = true;
                    txtAzurePAT.Visible = true;

                    lblAzuretestplanId.Visible = true;
                    txtAzuretestplanId.Visible = true;

                    lblAzuretestsuiteId.Visible = true;
                    txtAzuretestsuiteId.Visible = true;

                    lblAzureSettingsId.Visible = true;
                    txtAzureSettingsID.Visible = true;

                    lblAzureconfigId.Visible = true;
                    txtAzureconfigId.Visible = true;

                    lblAzureEnvironment.Visible = true;
                    txtAzureEnvironment.Visible = true;

                    lblMTMenvironment.Visible = false;
                    txtMTMenvironment.Visible = false;


                    lblMTMconfigId.Visible = false;
                    txtMTMconfigID.Visible = false;

                    lblMTMsettingsId.Visible = false;
                    txtMTMsettingsID.Visible = false;

                    lblMTMsuiteId.Visible = false;
                    txtMTMtestsuiteId.Visible = false;

                    lblMTMtestplanid.Visible = false;
                    txtMTMtestplanId.Visible = false;

                    lblqTestserver.Visible = false;
                    txtqTest.Visible = false;

                    lblmtmserver.Visible = false;
                    txtmtm.Visible = false;

                    lblqTestUSerName.Visible = false;
                    txtqtestUserName.Visible = false;

                    lblMTMUserName.Visible = false;
                    txtMTMUserName.Visible = false;

                    lblMTMpassword.Visible = false;
                    txtMTMPassword.Visible = false;


                    lblqtestPassword.Visible = false;
                    txtqtestPassword.Visible = false;

                    SetAzureDevOpsConncetionDetaiSettings(azureDevOpsConnectionDetails);
                    break;
                   
                case "qTest":
                    lblqTestserver.Visible = true;
                    txtqTest.Visible = true;

                    lblqTestUSerName.Visible = true;
                    txtqtestUserName.Visible = true;

                    lblqtestPassword.Visible = true;
                    txtqtestPassword.Visible = true;

                    lblMTMenvironment.Visible = false;
                    txtMTMenvironment.Visible = false;


                    lblAzureEnvironment.Visible = false;
                    txtAzureEnvironment.Visible = false;

                    lblMTMconfigId.Visible = false;
                    txtMTMconfigID.Visible = false;

                    lblAzureconfigId.Visible = false;
                    txtAzureconfigId.Visible = false;

                    lblMTMsettingsId.Visible = false;
                    txtMTMsettingsID.Visible = false;

                    lblAzureSettingsId.Visible = false;
                    txtAzureSettingsID.Visible = false;

                    lblMTMsuiteId.Visible = false;
                    txtMTMtestsuiteId.Visible = false;

                    lblAzuretestsuiteId.Visible = false;
                    txtAzuretestsuiteId.Visible = false;

                    lblAzuretestplanId.Visible = false;
                    txtAzuretestplanId.Visible = false;

                    lblMTMtestplanid.Visible = false;
                    txtMTMtestplanId.Visible = false;


                    lblmtmserver.Visible = false;
                    txtmtm.Visible = false;

                    lblazureserver.Visible = false;
                    txtazure.Visible = false;

                    lblMTMUserName.Visible = false;
                    txtMTMUserName.Visible = false;

                    lblAzurePAT.Visible = false;
                    txtAzurePAT.Visible = false;


                    lblMTMpassword.Visible = false;
                    txtMTMPassword.Visible = false;                    
                    break;
            }           
        }
        private void BtnSaveSchedulerSettings_Click(object sender, EventArgs e)
        {
            SchedulerSettings newSettings = new SchedulerSettings();

            newSettings.StartTime = dateTimeescheduler.Value.ToString("HH:mm", CultureInfo.CurrentCulture);

            if (rdbschedulerdaily.Checked)
            {
                newSettings.Frequency = "Daily";
                newSettings.Interval = 0;
            }
            else if (rdbschedulerhourly.Checked)
            {
                newSettings.Frequency = "Hourly";
                newSettings.Interval = Convert.ToInt32(cmbIntervals.SelectedItem);
            }

            try
            {
                settingsRepository.UpdateScheduler(string.Empty, newSettings);
                schedulerSettings = newSettings;
                MessageBox.Show("Configuration has been updated successfully !", "Success !");
            }
            catch (Exception ex)
            {
                MessageLogger.Log(ex);
                MessageBox.Show("Error while updating the Scheduler Configuration.", "Error !");
            }

            SendDataToServer(Commands.ServerSettings, schedulerSettings);
        }

        private void BtnCancelSchedulerSettings_Click(object sender, EventArgs e)
        {
            if (schedulerSettings == null)
            {
                schedulerSettings = new SchedulerSettings();
                schedulerSettings.Interval = 0;
                schedulerSettings.StartTime = "00:00";
                schedulerSettings.Frequency = "Hourly";
            }
            else
            {
                SetSchedulerSettings(schedulerSettings);
            }
        }

        public static ITestSuiteBase GetTestSuiteByName(ITestManagementTeamProject testManagementTeamProject, string suiteName)
        {
            string query = string.Concat("SELECT * FROM TestSuite where Title = '", suiteName, "'");
            var firstMatchingSuite = testManagementTeamProject.TestSuites.Query(query);

            return firstMatchingSuite.FirstOrDefault();
        }
        public List<string> GetTestPlans(string teamProject)
        {
            var tfs = TfsTeamProjectCollectionFactory
            .GetTeamProjectCollection(new Uri(ConfigurationManager.AppSettings["TfsUri"]));
            var service = tfs.GetService<ITestManagementService>();
            var testProject = service.GetTeamProject(teamProject);
            return testProject.TestPlans.Query("SELECT * FROM TestPlan").Select(i => i.Name).ToList();
        }
        public DataTable GetAzuredevOpsTestCases()
        {
            try
            {
                string serverurl = azureSettingsRepository.Get(string.Empty).ServerUrl;
                string project = azureSettingsRepository.Get(string.Empty).ProjectName;
                VssCredentials vssCred = new VssBasicCredential(string.Empty, azureSettingsRepository.Get(string.Empty).PersonalAccessToken);
                TfsTeamProjectCollection teamProjectCollection = new TfsTeamProjectCollection(new Uri(serverurl), vssCred);

                return FetchTestCases(teamProjectCollection, azureSettingsRepository.Get(string.Empty).ProjectName, azureSettingsRepository.Get(string.Empty).TestPlanId);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Message: " + ex.InnerException.ToString());
            }

            return null;
        }

        private DataTable FetchTestCases(TfsTeamProjectCollection teamProjectCollection, string projectName, int planId)
        {
            DataTable table = new DataTable();
            table.Columns.Add("TestCase Id", typeof(string));
            table.Columns.Add("TestCase Name", typeof(string));            

            ITestManagementService tms = teamProjectCollection.GetService<ITestManagementService>();
            var teamprojct = tms.GetTeamProject(projectName);
            string fullQuery =
                String.Format("SELECT [System.Id], [System.Title] FROM WorkItems WHERE [System.WorkItemType] = 'Test Case' AND [Team Project] = '{0}'", teamprojct.TeamProjectName);

            IEnumerable<ITestCase> allTestCases = teamprojct.TestCases.Query(fullQuery);

            foreach (var currentTestCase in allTestCases)
            {
                table.Rows.Add(currentTestCase.Id.ToString(), currentTestCase.Title);            
            }                         
            return table;
        }

        public void GetAzuredevOpsTestCasesResult()
        {
            try
            {
                string serverurl = azureSettingsRepository.Get(string.Empty).ServerUrl;
                string project = azureSettingsRepository.Get(string.Empty).ProjectName;
                VssCredentials vssCred = new VssBasicCredential(string.Empty, azureSettingsRepository.Get(string.Empty).PersonalAccessToken);
                TfsTeamProjectCollection teamProjectCollection = new TfsTeamProjectCollection(new Uri(serverurl), vssCred);
                
                FetchResult(teamProjectCollection, azureSettingsRepository.Get(string.Empty).ProjectName, azureSettingsRepository.Get(string.Empty).TestPlanId);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Message: " + ex.InnerException.ToString());
            }
        }

        private void FetchResult(TfsTeamProjectCollection teamProjectCollection, string projectName, int planId)
        {
            ITestManagementService tms = teamProjectCollection.GetService<ITestManagementService>();

            var teamprojct = tms.GetTeamProject(projectName);

            string fullQuery =
                String.Format("SELECT [System.Id], [System.Title] FROM WorkItems WHERE [System.WorkItemType] = 'Test Case' AND [Team Project] = '{0}'", teamprojct.TeamProjectName);

            IEnumerable<ITestCase> allTestCases = teamprojct.TestCases.Query(fullQuery);

            List<ITestCaseResultCollection> testCaseResults = null;

                if (cmbboxtestresults.SelectedItem.ToString().Equals("All"))
                {
                    testCaseResults = GetAllTestCaseResultsByDateTime(tms.GetTeamProject(projectName), planId);
                }
                else
                {

                   testCaseResults = GetSpecificTestCaseResultsByDateTime(tms.GetTeamProject(projectName), planId);

                }
           
            //var testCaseResults = GetAllTestCaseResults(tms.GetTeamProject(projectName), planId);

            DataTable table = new DataTable();
            table.Columns.Add("Id", typeof(string));
            table.Columns.Add("Owner Name", typeof(string));
            table.Columns.Add("Execution Date", typeof(string));
            table.Columns.Add("Result", typeof(string));

            foreach (ITestCaseResultCollection collection in testCaseResults)
            {
                foreach (var testResult in collection)
                {
                    table.Rows.Add(testResult.TestCaseId.ToString(), testResult.OwnerName, testResult.LastUpdated.ToString(), testResult.Outcome.ToString());
                }
            }
            dataGridView1.DataSource = null;
            if (table.Rows.Count == 0) { MessageBox.Show("No Test Result found against the TestId : " + cmbboxtestresults.SelectedItem.ToString(),"Test Report", MessageBoxButtons.OK, MessageBoxIcon.Information); }
            else { dataGridView1.DataSource = table; }
            
        }

        public List<ITestCaseResultCollection> GetAllTestCaseResults(ITestManagementTeamProject testproject, int testplanId)
        {
            ITestCaseResultCollection testCaseResult = null;
            List<ITestCaseResultCollection> testCaseResults = new List<ITestCaseResultCollection>();
            var plans = testproject.TestPlans.Query("Select * from TestPlan");
            var planById = plans.First(x => x.Id == testplanId);

            foreach (ITestSuiteEntry suiteEntry in planById.RootSuite.Entries)
            {
                var testSuite = suiteEntry.TestSuite;
                foreach (ITestSuiteEntry subSuiteEntry in testSuite.TestCases)
                {
                    testCaseResult = testproject.TestResults.ByTestId(subSuiteEntry.TestCase.Id);
                    testCaseResults.Add(testCaseResult);
                }
            }

            return testCaseResults;
        }

        public List<ITestCaseResultCollection> GetSpecificTestCaseResultsByDateTime(ITestManagementTeamProject testproject, int testplanId)
        {
            ITestCaseResultCollection testCaseResult = null;
            List<ITestCaseResultCollection> testCaseResults = new List<ITestCaseResultCollection>();
            var plans = testproject.TestPlans.Query("Select * from TestPlan");
            var planById = plans.First(x => x.Id == testplanId);
            string query = "SELECT * FROM TestResult WHERE TestCaseId=" + Convert.ToInt32(cmbboxtestresults.SelectedItem.ToString().Substring(2)) + " AND DateCompleted > '" + dateTimePickerStart.Value + "' And DateCompleted < '" + dateTimePickerEnd.Value + "' order by LastUpdated";
            testCaseResult =  testproject.TestResults.Query(query);
            testCaseResults.Add(testCaseResult);
            return testCaseResults;            
        }

        public List<ITestCaseResultCollection> GetAllTestCaseResultsByDateTime(ITestManagementTeamProject testproject, int testplanId)
        {
            ITestCaseResultCollection testCaseResult = null;
            List<ITestCaseResultCollection> testCaseResults = new List<ITestCaseResultCollection>();
            var plans = testproject.TestPlans.Query("Select * from TestPlan");
            var planById = plans.First(x => x.Id == testplanId);
            string query = "SELECT * FROM TestResult WHERE DateCompleted > '" + dateTimePickerStart.Value + "' And DateCompleted < '" + dateTimePickerEnd.Value + "' order by TestCaseId,LastUpdated";
            testCaseResult = testproject.TestResults.Query(query);
            testCaseResults.Add(testCaseResult);
            return testCaseResults;           
        }
        public void GetMTMTestCasesResult()
        {

            try
            {
                string serverurl = tfsSettingsRepository.Get(string.Empty).ServerUrl;
                NetworkCredential credential = new NetworkCredential(tfsSettingsRepository.Get(string.Empty).ServerUserName, tfsSettingsRepository.Get(string.Empty).ServerPassword);
                Microsoft.VisualStudio.Services.Common.WindowsCredential winCred = new Microsoft.VisualStudio.Services.Common.WindowsCredential(credential);
                VssCredentials vssCred = new VssCredentials(winCred);
                TfsTeamProjectCollection teamProjectCollection = new TfsTeamProjectCollection(new Uri(serverurl), vssCred);
                FetchResult(teamProjectCollection, tfsSettingsRepository.Get(string.Empty).ProjectName, tfsSettingsRepository.Get(string.Empty).TestPlanId);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Message: " + ex.InnerException.ToString());
            }

        }
              
        private void UpdateTestStatusinMTM(int testPlanId, int testCaseId, int test_config, string testStatus)
        {

            int result_updated = 0;
            TextWriter tw = new StreamWriter(@"C:\Users\*\Desktop\MTM.txt", true);

            tw.WriteLine("Attempting the test case " + testCaseId + "to be marked as " + test_config);

            //Create TFS connection
            TfsTeamProjectCollection tfs = new TfsTeamProjectCollection(TfsTeamProjectCollection.GetFullyQualifiedUriForName("<tfs_url>"));

            ITestManagementService tms = tfs.GetService<ITestManagementService>();

            //Connect to the project
            ITestManagementTeamProject proj = tms.GetTeamProject("<project>");

            ITestPlan Plan = proj.TestPlans.Find(testPlanId);

            if (Plan != null)
            {
                ITestCase tc = proj.TestCases.Find(testCaseId);
                if (tc != null)
                {
                    ITestPointCollection points = Plan.QueryTestPoints("SELECT * FROM TestPoint WHERE TestCaseId =" + testCaseId);


                    foreach (ITestPoint p in points)
                    {

                        var testResults = proj.TestResults.ByTestId(testCaseId);
                        foreach (ITestCaseResult result in testResults)
                        {
                            if (p.ConfigurationId == test_config)
                            {
                                if (testStatus == "Failed")
                                {
                                    result.Outcome = TestOutcome.Failed;
                                }
                                else if (testStatus == "Passed")
                                {
                                    Console.WriteLine("Passed");
                                    result.Outcome = TestOutcome.Passed;
                                }
                                else if (testStatus == "NotExecuted")
                                {
                                    result.Outcome = TestOutcome.NotExecuted;
                                }
                                result.Save(true);
                                result_updated = 1;
                                tw.WriteLine("Test case ID =>" + testCaseId + "is marked as " + testStatus + "MTM");
                            }
                        }
                        if (result_updated == 1) { break; }
                    }
                    if (result_updated == 0)
                    {
                        tw.WriteLine(test_config + " => Configuration is not found in the test case " + testCaseId);
                    }
                }
                else
                {
                    tw.WriteLine(testCaseId + " Test case not found the plan");
                }
            }
            else
            {
                tw.WriteLine(testPlanId + " Plan not found in MTM");
            }
            tw.Close();
            //end
        }

        //BtnSave_Click(Save Button) in Test Server Setting tab saves the user information  in the field provided.
        //Selecting the cmbtestexecution (ComboBox) and providing input to the respective fields.

        private void BtnSave_Click(object sender, EventArgs e)
        {
            if (cmbtestexecution.SelectedItem.Equals(tfsSettingsRepository.Get(string.Empty).TestServerType.Split(',')[0]))
            {
                UpdateMTMTestDetails();
            }
            else if (cmbtestexecution.SelectedItem.Equals(tfsSettingsRepository.Get(string.Empty).TestServerType.Split(',')[1]))
            {
                UpdateAzureDevOpsTestDetails();
            }
            else if (cmbtestexecution.SelectedItem.Equals(tfsSettingsRepository.Get(string.Empty).TestServerType.Split(',')[2]))
            {
                UpdateQTestDetails();
            }
        }
        //Update the fields in MTM Repository provided in Test Server Setting tab.
        private void UpdateMTMTestDetails()
        {
            TfsConnectionDetails newSettings = new TfsConnectionDetails();
            newSettings.ServerUrl = txtmtm.Text;
            newSettings.ServerUserName = txtMTMUserName.Text;
            newSettings.ServerPassword = txtMTMPassword.Text;
            newSettings.TestPlanId = Convert.ToInt32(txtMTMtestplanId.Text);
            newSettings.TestSuiteId = Convert.ToInt32(txtMTMtestsuiteId.Text);
            newSettings.TestSettingId = Convert.ToInt32(txtMTMsettingsID.Text);
            newSettings.ConfigurationId = Convert.ToInt32(txtMTMconfigID.Text);
            newSettings.EnvironmentName = txtMTMenvironment.Text;

            try
            {
                tfsSettingsRepository.Update(string.Empty, newSettings);
                tfsConnectionDetails = newSettings;
                MessageBox.Show("Configuration has been updated successfully !", "Success !");
            }
            catch (Exception ex)
            {
                MessageLogger.Log(ex);
                MessageBox.Show("Error while updating the Scheduler Configuration.", "Error !");
            }

        }
        //Update the fields in AzureDevops Repository provided in Test Server Setting tab.
        private void UpdateAzureDevOpsTestDetails()
        {

            AzureDevOpsConnectionDetails newSettings = new AzureDevOpsConnectionDetails();
            newSettings.ServerUrl = txtazure.Text;
            newSettings.PersonalAccessToken = txtAzurePAT.Text;
            newSettings.TestPlanId = Convert.ToInt32(txtAzuretestplanId.Text);
            newSettings.TestSuiteId = Convert.ToInt32(txtAzuretestsuiteId.Text);
            newSettings.TestSettingId = Convert.ToInt32(txtAzureSettingsID.Text);
            newSettings.ConfigurationId = Convert.ToInt32(txtAzureconfigId.Text);
            newSettings.EnvironmentName = txtAzureEnvironment.Text;

            try
            {
                azureSettingsRepository.Update(string.Empty, newSettings);
                azureDevOpsConnectionDetails = newSettings;
                MessageBox.Show("Configuration has been updated successfully !", "Success !");
            }
            catch (Exception ex)
            {
                MessageLogger.Log(ex);
                MessageBox.Show("Error while updating the Scheduler Configuration.", "Error !");
            }

        }

        private void UpdateQTestDetails()
        {

            TfsConnectionDetails newSettings = new TfsConnectionDetails();
            newSettings.ServerUrl = txtmtm.Text;
            newSettings.ServerUserName = txtMTMUserName.Text;
            newSettings.ServerPassword = txtMTMPassword.Text;

            try
            {
                tfsSettingsRepository.Update(string.Empty, newSettings);
                tfsConnectionDetails = newSettings;
                MessageBox.Show("Configuration has been updated successfully !", "Success !");
            }
            catch (Exception ex)
            {
                MessageLogger.Log(ex);
                MessageBox.Show("Error while updating the Scheduler Configuration.", "Error !");
            }

        }

        private void SetTFSConncetionDetaiSettings(TfsConnectionDetails settings)
        {
            if (settings == null)
                settings = tfsSettingsRepository.Get(string.Empty);
            if (cmbtestexecution.SelectedItem.Equals("MTM"))
            {
                txtmtm.Text = settings.ServerUrl;
                txtMTMUserName.Text = settings.ServerUserName;
                txtMTMPassword.Text = settings.ServerPassword;
                txtMTMtestplanId.Text = settings.TestPlanId.ToString();
                txtMTMtestsuiteId.Text = settings.TestSuiteId.ToString();
                txtMTMsettingsID.Text = settings.TestSettingId.ToString();
                txtMTMconfigID.Text = settings.ConfigurationId.ToString();
                txtMTMenvironment.Text = settings.EnvironmentName;
            }
        }

        private void SetAzureDevOpsConncetionDetaiSettings(AzureDevOpsConnectionDetails settings)
        {
            if (settings == null)
                settings = azureSettingsRepository.Get(string.Empty);
            if (cmbtestexecution.SelectedItem.Equals("AzureDevOps"))
            {
                txtazure.Text = settings.ServerUrl;
                txtAzurePAT.Text = settings.PersonalAccessToken;                
                txtAzuretestplanId.Text = settings.TestPlanId.ToString();
                txtAzuretestsuiteId.Text = settings.TestSuiteId.ToString();
                txtAzureSettingsID.Text = settings.TestSettingId.ToString();
                txtAzureconfigId.Text = settings.ConfigurationId.ToString();
                txtAzureEnvironment.Text = settings.EnvironmentName;
            }
        }
        private void ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {   
        }

        //BtnCancel_Click (Cancel Button) in Test Server Setting tab provides the last entered value as default
        // If the BtnSave_Click (Save) is not enterd
        private void BtnCancel_Click(object sender, EventArgs e)
        {
            if (cmbtestexecution.SelectedItem.Equals("AzureDevOps"))
            {
                SetAzureDevOpsConncetionDetaiSettings(azureDevOpsConnectionDetails);
            }
            else
            {
                SetTFSConncetionDetaiSettings(tfsConnectionDetails);
            }
        }

        //Enables to select a Date Range in Test Execution Result tab to run specific Test Cases  
        //dateTimePickerStart implies (Start Date ) & dateTimePickerEnd implies (End Data)
        private void DateTimePickerEnd_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePickerStart.Value > dateTimePickerEnd.Value)
            {
                MessageBox.Show("End date cannot be prior to the  Start date");
                dateTimePickerEnd.Value = Convert.ToDateTime(dateTimePickerStart);

            }
        }

        private void LvTests_SelectedIndexChanged(object sender, EventArgs e)
        {
           // MessageBox.Show(this.lvTests.Items.Count.ToString());
        }

        private void BtnRun_Click(object sender, EventArgs e)
        {
            if (cmbtestexecution.SelectedItem.Equals(tfsSettingsRepository.Get(string.Empty).TestServerType.Split(',')[0]))
            {
                GetMTMTestCasesResult();
            }
            else if (cmbtestexecution.SelectedItem.Equals(tfsSettingsRepository.Get(string.Empty).TestServerType.Split(',')[1]))
            {
                GetAzuredevOpsTestCasesResult();
            }

        }

        private void LblestCaseId_Click(object sender, EventArgs e)
        {

        }

        //Btnexecute_Click(Run button) is executed depending on the selection of cmbtestexecution (ComboBox)
        //Depending on the Selection either RunMTMTestCasesResult() Or RunAzuredevOpsTestCasesResult() is executed
        private void Btnexecute_Click(object sender, EventArgs e)
        {
            if (cmbtestexecution.SelectedItem.Equals(tfsSettingsRepository.Get(string.Empty).TestServerType.Split(',')[0]))
            {
                RunMTMTestCasesResult();
            }
            else if (cmbtestexecution.SelectedItem.Equals(tfsSettingsRepository.Get(string.Empty).TestServerType.Split(',')[1]))
            {
                RunAzuredevOpsTestCasesResult();
            }
        }

        //Responsible for executing the Test Cases of MTM Repository once the fields are provided.
        //Goto==>TestServerSetting tab ==> Select MTM from the Repository Name ComboBox ==> Provide the fields ==> Enter Run button
         private void RunMTMTestCasesResult()
         {            
            string serverurl = tfsSettingsRepository.Get(string.Empty).ServerUrl;
            NetworkCredential credential = new NetworkCredential(tfsSettingsRepository.Get(string.Empty).ServerUserName, tfsSettingsRepository.Get(string.Empty).ServerPassword);
            Microsoft.VisualStudio.Services.Common.WindowsCredential winCred = new Microsoft.VisualStudio.Services.Common.WindowsCredential(credential);
            VssCredentials vssCred = new VssCredentials(winCred);
            TfsTeamProjectCollection teamProjectCollection = new TfsTeamProjectCollection(new Uri(serverurl), vssCred);

            teamProjectCollection.EnsureAuthenticated();

            ITestManagementService testManagementService = teamProjectCollection.GetService<ITestManagementService>();
            ITestManagementTeamProject project = testManagementService.GetTeamProject(tfsSettingsRepository.Get(string.Empty).ProjectName);

            //Get user name
            TeamFoundationIdentity tfi = testManagementService.AuthorizedIdentity;

            ITestPlan plan = project.TestPlans.Find(tfsSettingsRepository.Get(string.Empty).TestPlanId);
            ITestSuiteBase suite = project.TestSuites.Find(tfsSettingsRepository.Get(string.Empty).TestSuiteId);
            ITestSettings testSettings = project.TestSettings.Find(tfsSettingsRepository.Get(string.Empty).TestSettingId);
            ITestConfiguration testConfiguration = project.TestConfigurations.Find(tfsSettingsRepository.Get(string.Empty).ConfigurationId);

            // Unfortunately test environment name is not exactly the name you see in MTM.
            // In order to get the name of your environments just call
            // project.TestEnvironments.Query()
            // set a breakpoint, run this code in debuger and check the names.      
            ITestEnvironment testEnvironment = project.TestEnvironments.Find((from te in project.TestEnvironments.Query()
                                                                              where te.Name.ToUpper().Equals(tfsSettingsRepository.Get(string.Empty).EnvironmentName.ToUpper())
                                                                              select te.Id).SingleOrDefault());

            ITestRun testRun = plan.CreateTestRun(true);
            testRun.Owner = tfi;
            testRun.Controller = testEnvironment.ControllerName;
            testRun.CopyTestSettings(testSettings);
            testRun.TestEnvironmentId = testEnvironment.Id;
            testRun.Title = "Tests started from the dashboard";

            //Get test points
            ITestPointCollection testpoints = plan.QueryTestPoints("SELECT * FROM TestPoint WHERE SuiteId = " + suite.Id + " and ConfigurationId = " + testConfiguration.Id);

            foreach (ITestPoint tp in testpoints)
            {
                testRun.AddTestPoint(tp, tfi);
            }

            // This call starts your tests!
            testRun.Save();
        }

        //Responsible for executing the Test Cases of AzureDevops Repository once the fields are provided.
        //Goto==>TestServerSetting tab ==> Select AzureDevops from the Repository Name ComboBox ==> Provide the fields ==> Enter Run button
        private void RunAzuredevOpsTestCasesResult()
        {
            string serverurl = azureSettingsRepository.Get(string.Empty).ServerUrl;            
            VssCredentials vssCred = new VssBasicCredential(string.Empty, azureSettingsRepository.Get(string.Empty).PersonalAccessToken);
            TfsTeamProjectCollection teamProjectCollection = new TfsTeamProjectCollection(new Uri(serverurl), vssCred);

            teamProjectCollection.EnsureAuthenticated();

            ITestManagementService testManagementService = teamProjectCollection.GetService<ITestManagementService>();
            ITestManagementTeamProject project = testManagementService.GetTeamProject(azureSettingsRepository.Get(string.Empty).ProjectName);

            //Get user name
            TeamFoundationIdentity tfi = testManagementService.AuthorizedIdentity;

            ITestPlan plan = project.TestPlans.Find(azureSettingsRepository.Get(string.Empty).TestPlanId);
            ITestSuiteBase suite = project.TestSuites.Find(azureSettingsRepository.Get(string.Empty).TestSuiteId);
            ITestSettings testSettings = project.TestSettings.Find(azureSettingsRepository.Get(string.Empty).TestSettingId);
            ITestConfiguration testConfiguration = project.TestConfigurations.Find(azureSettingsRepository.Get(string.Empty).ConfigurationId);

            // Unfortunately test environment name is not exactly the name you see in MTM.
            // In order to get the name of your environments just call
            // project.TestEnvironments.Query()
            // set a breakpoint, run this code in debuger and check the names.      
            ITestEnvironment testEnvironment = project.TestEnvironments.Find((from te in project.TestEnvironments.Query()
                                                                              where te.Name.ToUpper().Equals(azureSettingsRepository.Get(string.Empty).EnvironmentName.ToUpper())
                                                                              select te.Id).SingleOrDefault());

            ITestRun testRun = plan.CreateTestRun(true);
            testRun.Owner = tfi;
            testRun.Controller = testEnvironment.ControllerName;
            testRun.CopyTestSettings(testSettings);
            testRun.TestEnvironmentId = testEnvironment.Id;
            testRun.Title = "Tests started from the dashboard";

            //Get test points
            ITestPointCollection testpoints = plan.QueryTestPoints("SELECT * FROM TestPoint WHERE SuiteId = " + suite.Id + " and ConfigurationId = " + testConfiguration.Id);

            foreach (ITestPoint tp in testpoints)
            {
                testRun.AddTestPoint(tp, tfi);
            }

            // This call starts your tests!
            testRun.Save();
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void panel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void lblqTestUSerName_Click(object sender, EventArgs e)
        {

        }

        private void lblAzurePAT_Click(object sender, EventArgs e)
        {

        }

        private void lblqtestPassword_Click(object sender, EventArgs e)
        {

        }

        private void lblMTMtestplanid_Click(object sender, EventArgs e)
        {

        }

        private void lblMTMsuiteId_Click(object sender, EventArgs e)
        {

        }

        private void txtMTMUserName_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtqtestUserName_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtAzurePAT_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
