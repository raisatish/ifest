﻿using Injections;
using Interfaces;
using System.Windows.Forms;

namespace IfestTool
{
    public class BaseForm : Form
    {
        protected ILog MessageLogger { get; set; }

        public BaseForm()
        {
            MessageLogger = Injector.GetObject<ILog>();
        }
    }
}
