﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;

namespace Santendar
{
    [TestClass]
    public class DriverScript
    {
        IWebDriver driver;

        [TestMethod]
        public void TestCaseExecution()
        {
            Console.WriteLine("Test case Execution is started");
            InitializeTests("IE");
            MaximizeWindow();
            NavigateURL();
           
        }

        public void InitializeTests(String Browser)
        {
            switch (Browser)
            {
                case "FF":
                    Console.WriteLine("Setting up the Firefox Driver");
                    driver = new FirefoxDriver();
                    break;
                case "GC":
                    Console.WriteLine("Setting up the Chrome Driver");
                    driver = new ChromeDriver();
                    break;
                case "IE":
                    Console.WriteLine("Setting up the Internet Explorer Driver");
                    driver = new InternetExplorerDriver();
                    break;
                default:
                    Console.WriteLine("Default case");
                    break;
            }
        }

        public void MaximizeWindow()
        {
            Console.WriteLine("Window Maximized");
            driver.Manage().Window.Maximize();

        }

        public void NavigateURL()
        {
            Console.WriteLine("Navigating to Google Site");
            driver.Navigate().GoToUrl("https://www.google.co.in");
        }

        public void TestCase1()
        {
            Console.WriteLine("Test Case1");
        }
        public void TestCase2()
        {
            Console.WriteLine("Test Case2");
        }
        public void TestCase3()
        {
            Console.WriteLine("Test Case3");
        }

        [TearDown]
        public void cleanup()
        {
            driver.Close();
        }
    }
}
