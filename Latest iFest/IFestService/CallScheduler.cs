﻿using Injections;
using Interfaces;
using Models;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Timers;
using System.Xml;

namespace IFestService
{
    public class CallScheduler
    {
        public bool Paused { get; set; }

        public bool ShutdownInprogress { get; set; }

        public bool TaskInProgress { get; set; }

        private Timer timer { get; set; }

        private ILog logger { get; set; }

        public static string freq;
        private ISourceVersionControl CodeRepository { get; set; }

        private ICommServer CommunicationServer { get; set; }

        private  SchedulerSettings ServerSettings { get; set; }

        private bool NextOccurranceSet = false;
        private DateTime NextOccurrance;

        public CallScheduler()
        {
            Paused = false;
            ShutdownInprogress = false;
            TaskInProgress = false;
            timer = new Timer();
            logger = Injector.GetObject<ILog>();
            CodeRepository = Injector.GetObject<ISourceVersionControl>();

            CommunicationServer = Injector.GetObject<ICommServer>();
        }

        public void Start()
        {
            try
            {
                CommunicationServer.GotDataFromClient += CommunicationServer_GotDataFromClient;
                CommunicationServer.Start();

                timer.Interval = 60000; // TODO: for now this is 10 sec. Later should be changed to 1 minute (60000)
                timer.Elapsed += new ElapsedEventHandler(OnTimer);
                timer.Start();

                logger.Log("Service Started...");
            }
            catch (Exception ex)
            {
                logger.Log(ex);
            }
        }

        private void CommunicationServer_GotDataFromClient(object sender, string msg)
        {
            logger.Log("CallScheduler: Data received - " + msg);

            dynamic mo = JsonConvert.DeserializeObject(msg);

            try
            {
                string CommandName = mo.Command;
                if (CommandName == Commands.ServerSettings)
                {
                    ServerSettings = JsonConvert.DeserializeObject<SchedulerSettings>(mo.CommandData.ToString());

                    if (ServerSettings == null)
                        throw new Exception("Invalid scheduler settings received.");

                    CodeRepository.ServerUrl(ServerSettings.ServerUrl);
                    CodeRepository.SetAccessToken(ServerSettings.ServerAccessToken);
                    CodeRepository.ProjectName(ServerSettings.ProjectName);
                    var strDtTm = DateTime.Today.ToString("yyyy-MM-dd") + " " + ServerSettings.StartTime;
                    DateTime dtTmStart;
                    int loopCount = 0;
                    if (DateTime.TryParse(strDtTm, out dtTmStart))
                    {
                        freq = ServerSettings.Frequency;
                        if (freq.Equals("Daily"))
                        {
                            while (true)
                            {
                                dtTmStart = getUpdateTime();
                                if (dtTmStart.ToString("MM/dd/yyyy hh mm") == DateTime.Now.ToString("MM/dd/yyyy hh mm"))
                                {
                                    NextOccurrance = dtTmStart.AddMilliseconds(ServerSettings.Interval.Value * 1);
                                    break;
                                }
                                freq = ServerSettings.Frequency;
                                if (!freq.Equals("Daily"))
                                {
                                    break;
                                }
                            }
                            NextOccurranceSet = true;
                        }
                        else
                        {
                            NextOccurrance = dtTmStart;
                            NextOccurrance = NextOccurrance.AddHours(ServerSettings.Interval.Value);
                            while (true)
                            {

                                if (NextOccurrance.ToString("MM/dd/yyyy hh mm") == DateTime.Now.ToString("MM/dd/yyyy hh mm"))
                                {
                                    NextOccurrance = dtTmStart.AddMilliseconds(ServerSettings.Interval.Value * 1);
                                    break;
                                }
                                freq = ServerSettings.Frequency;
                                if (freq.Equals("Daily"))
                                {
                                    break;
                                }
                            }

                            NextOccurranceSet = true;
                        }
                        //if (dtTmStart > DateTime.Now)
                        //{
                        //    //if (ServerSettings.Frequency.Equals("Daily", StringComparison.InvariantCultureIgnoreCase))
                        //    //{
                        //    //    NextOccurrance = dtTmStart;
                        //    //    NextOccurranceSet = true;
                        //    //}
                        //    //else if (ServerSettings.Frequency.Equals("Hourly", StringComparison.InvariantCultureIgnoreCase))
                        //    {
                        //        while(dtTmStart.AddHours(ServerSettings.Interval.Value * -1) > DateTime.Now && loopCount < 100)
                        //        {
                        //            NextOccurrance = dtTmStart.AddHours(ServerSettings.Interval.Value * -1);
                        //            loopCount++;
                        //        }

                        //        NextOccurranceSet = true;
                        //    }
                        //}
                        //else
                        //{
                        //    NextOccurrance = dtTmStart;

                        //    while (NextOccurrance < DateTime.Now && loopCount < 100)
                        //    {
                        //        NextOccurrance = NextOccurrance.AddHours(ServerSettings.Interval.Value);
                        //        loopCount++;
                        //    }

                        //    NextOccurranceSet = true;
                        //}
                    }
                }

                return;
            }
            catch (Microsoft.CSharp.RuntimeBinder.RuntimeBinderException be)
            {
                //
            }
        }

        public DateTime getUpdateTime()
        {

            XmlDataDocument xmldoc = new XmlDataDocument();
            XmlNodeList xmlnode;
            int i = 0;
            string str = null;
            FileStream fs = new FileStream("..\\..\\..\\IfestTool\\SchedulerConfig.xml", FileMode.Open, FileAccess.Read);
            xmldoc.Load(fs);
            xmlnode = xmldoc.GetElementsByTagName("StartTime");
            DateTime freq = DateTime.Parse(xmlnode[0].ChildNodes.Item(0).InnerText.Trim().ToString());
            fs.Close();
            return freq;

        }
        private void OnTimer(object sender, ElapsedEventArgs e)
        {
            try
            {
                if (Paused || ShutdownInprogress)
                    return;

                if (NextOccurranceSet)
                {
                    if (NextOccurrance > DateTime.Now)
                        return;
                }
                else
                    return;

                NextOccurranceSet = false;

                TaskInProgress = true;
                //TODO: This number should be replaced with dynamic value from application
                var modifiedClasses = CodeRepository.GetModifiedClassesList(ServerSettings.ChangesetNo);
                CommunicationServer.SendCommandWithData(Commands.ModifiedClassesList, string.Join(", ", modifiedClasses));
                //   CommunicationServer.SendCommandWithData(Commands.UpdateChangeset, CodeRepository.GetLatestProcessedChangesetNo().ToString());
                CommunicationServer.SendCommandWithData(Commands.UpdateChangeset, CodeRepository.GetLatestProcessedCommitID().ToString());
                
                TaskInProgress = false;

                NextOccurrance = NextOccurrance.AddHours(ServerSettings.Interval.Value);
                NextOccurranceSet = true;
            }
            catch (Exception ex)
            {
                logger.Log(ex);
                TaskInProgress = false;
            }
        }

        public void Stop()
        {

        }
    }
}
