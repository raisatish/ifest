﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace IFestService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            if (Environment.UserInteractive)
            {
                var cs = new CallScheduler();
                cs.Start();

                Console.Write("\n\n\tiFest service up and running..!\n\n\n\tPress Enter to Exit...");
                Console.ReadLine();
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                new SvcKernel()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
