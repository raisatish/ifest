﻿using System.ComponentModel;
using System.Configuration.Install;

namespace IFestService
{
    [RunInstaller(true)]
    public partial class InstallSvcProj : Installer
    {
        public InstallSvcProj()
        {
            InitializeComponent();
        }
    }
}
