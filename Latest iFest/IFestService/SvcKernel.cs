﻿using System.ServiceProcess;

namespace IFestService
{
    public partial class SvcKernel : ServiceBase
    {
        private CallScheduler callScheduler;
        public SvcKernel()
        {
            InitializeComponent();

            CanShutdown = true;
            CanPauseAndContinue = true;
        }

        protected override void OnStart(string[] args)
        {
            callScheduler = new CallScheduler();
            callScheduler.Start();

            base.OnStart(args);
        }

        protected override void OnStop()
        {
            callScheduler.Stop();

            base.Stop();
        }

        protected override void OnPause()
        {
            callScheduler.Paused = true;

            base.OnPause();
        }

        protected override void OnContinue()
        {
            callScheduler.Paused = false;

            base.OnContinue();
        }

        protected override void OnShutdown()
        {
            callScheduler.ShutdownInprogress = true;

            // Wait for 4 seconds to see if task completes
            int retryCount = 0;
            while (callScheduler.TaskInProgress || retryCount++ < 200)
                System.Threading.Thread.Sleep(200);

            base.OnShutdown();
        }
    }
}
