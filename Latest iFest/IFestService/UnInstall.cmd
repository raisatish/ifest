﻿@echo off
echo Un-Installing WindowsService...
echo ---------------------------------------------------
REM The following directory is for .NET 4.0
%SystemRoot%\Microsoft.NET\Framework64\v4.0.30319\InstallUtil /u IFestService.exe
echo ---------------------------------------------------
echo Waiting for the un-install to complete
timeout 3
echo Done.