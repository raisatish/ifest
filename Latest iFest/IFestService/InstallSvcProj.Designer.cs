﻿namespace IFestService
{
    partial class InstallSvcProj
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ProcInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.SvcInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // ProcInstaller
            // 
            this.ProcInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.ProcInstaller.Password = null;
            this.ProcInstaller.Username = null;
            // 
            // SvcInstaller
            // 
            this.SvcInstaller.DelayedAutoStart = true;
            this.SvcInstaller.Description = "Intelligently filter, schedule and execute tests to improve quality of product.";
            this.SvcInstaller.DisplayName = "Ifest Service";
            this.SvcInstaller.ServiceName = "Ifest";
            this.SvcInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // InstallSvcProj
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.ProcInstaller,
            this.SvcInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller ProcInstaller;
        private System.ServiceProcess.ServiceInstaller SvcInstaller;
    }
}