﻿namespace Models
{
    public class TfsGitChangesetListItem
    {
        public string name;
        public string url;
        public string id;
        public string commitId;
        public string date;
    }
}