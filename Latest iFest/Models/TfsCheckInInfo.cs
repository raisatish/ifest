﻿namespace Models
{
    public class TfsCheckInInfo
    {
        public string id;
        public string displayName;
        public string uniqueName;
        public string url;
        public string imageUrl;
    }
}
