﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class TestCaseDialog
    {
        public bool isSelected { get; set; }
        public string TestCaseId { get; set; }
        public string TestCaseName { get; set; }
    }   
}
