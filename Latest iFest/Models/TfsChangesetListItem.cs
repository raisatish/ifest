﻿using System;
namespace Models
{
    public class TfsChangesetListItem
    {
        public int changesetId;
        public string url;
        public TfsAuthorInfo author;
        public TfsCheckInInfo checkedInBy;
        public DateTime createdDate;
    }
}
