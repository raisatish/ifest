﻿namespace Models
{
    public class TfsChangesetItemDetails
    {
        public int version;
        public int size;
        public string hashValue;
        public string path;
        public string url;
    }
}
