﻿namespace Models
{
    public class TfsAuthorInfo
    {
        public string id;
        public string displayName;
        public string uniqueName;
        public string url;
        public string imageUrl;
    }
}
