﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public partial class TFSGitClass
    {
        public string id { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public TfsProjectProperty tfsProjectProperty { get; set; }
        public string defaultBranch { get; set; }
        public string remoteUrl { get; set; }
    }
    public partial class TfsProjectProperty
    {
        public string id { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string state { get; set; }
        public string revision { get; set; }
    }
}
