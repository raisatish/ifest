﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Models
{
    [Serializable]
    [XmlRoot(ElementName = "Results")]
    public class SchExecResults
    {
        [XmlArrayItem("Result", typeof(SchExecResult))]
        public List<SchExecResult> Results { get; set; }
    }

    [Serializable]
    public class SchExecResult
    {
        public DateTime ExecDate { get; set; }

        [XmlArray("ModifiedClasses")]
        [XmlArrayItem("Class", typeof(DiffClass))]
        public List<DiffClass> ModifiedClasses { get; set; }
    }

    [Serializable]
    public class DiffClass
    {
        public string ClassName { get; set; }
        public string Tests { get; set; }
    }
}