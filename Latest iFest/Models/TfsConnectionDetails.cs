﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [Serializable]
    public class TfsConnectionDetails
    {
        public string ServerUrl { get; set; }
        public string ServerUserName { get; set; }
        public string ServerPassword { get; set; }               
        public string ProjectName { get; set; }
        public string TestServerType { get; set; }
        public int TestPlanId { get; set; }
        public int TestSuiteId { get; set; }
        public int TestSettingId { get; set; }
        public int ConfigurationId { get; set; }
        public string EnvironmentName { get; set; }      
    }
}
