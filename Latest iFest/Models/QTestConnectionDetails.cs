﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    [Serializable]
    public class QTestConnectionDetails
    {
        public string ServerUrl { get; set; }
        public string ServerUserName { get; set; }
        public string ServerPassword { get; set; }
    }
}
