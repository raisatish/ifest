﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public class AzureDevOpsConnectionDetails
    {
        public string ServerUrl { get; set; }
        public string PersonalAccessToken { get; set; }       
        public string ProjectName { get; set; }
        public string TestServerType { get; set; }
        public int TestPlanId { get; set; }      
        public int TestSuiteId { get; set; }
        public int TestSettingId { get; set; }
        public int ConfigurationId { get; set; }
        public string EnvironmentName { get; set; }
    }
}
