﻿namespace Models
{
    public class TfsChangesetListResponse
    {
        public int count;
        public TfsChangesetListItem[] value;
    }
}
