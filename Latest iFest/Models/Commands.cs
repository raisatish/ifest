﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    public static class Commands
    {
        public static string ServerSettings = "GetSchedulerSettings";
        public static string ModifiedClassesList = "ListOfModifiedClasses";
        public static string UpdateChangeset = "UpdateLatestChangesetNo";
    }
}
