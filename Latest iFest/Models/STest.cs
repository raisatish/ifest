﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Models
{
    [Serializable]
    [XmlRoot(ElementName = "Test")]
    public class STest
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string TestType { get; set; }
    }
}
