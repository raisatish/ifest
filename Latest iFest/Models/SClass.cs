﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Models
{
    [Serializable]
    [XmlRoot(ElementName = "Class")]
    public class SClass
    {
        public Guid Id { get; set; }
        public string Name { get; set; }

        [XmlArray("Tests")]
        [XmlArrayItem("Test", typeof(STest))]
        public List<STest> Tests { get; set; }
    }
}