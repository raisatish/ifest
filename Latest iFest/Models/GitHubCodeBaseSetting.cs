﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{   
    [Serializable]
    public class GitHubCodeBaseSetting
    {
        public string Frequency { get; set; }
        public string StartTime { get; set; }
        public int? Interval { get; set; }
        public string SourceControlType { get; set; }
        public string ChangesetNo { get; set; }
        public string ServerUrl { get; set; }
        public string ServerClientId { get; set; }
        public string ServerSecretKey { get; set; }
        public string ServerAuthenticationCode { get; set; }
        public string ProductName { get; set; }
        public string ColorCombination { get; set; }
        public string FilestoExclude { get; set; }

        public string gitPriviousCommitID;
    }
}
