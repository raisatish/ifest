﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Models
{
    [Serializable]
    public class TestsConfig
    {
        [XmlArray("Classes")]
        [XmlArrayItem("Class", typeof(SClass))]
        public List<SClass> Classes { get; set; }
    }
}
