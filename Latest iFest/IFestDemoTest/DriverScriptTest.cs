﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Reflection;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

using Logger;
using Injections;
using Interfaces;

namespace IFestDemoTest
{
    [TestClass]
    public class DriverScriptTest
    {
        protected ILog MessageLogger { get; set; }

        public DriverScriptTest()
        {
            MessageLogger = Injector.GetObject<ILog>();
        }

        TestCases TC = new TestCases();
        [TestMethod]
        public void TestMethod1()
        {
            Console.WriteLine("Calling test Methods 01");
            string lines = System.IO.File.ReadAllText(@"D:\TFSWorkSpace_Tool\Main\iFest\IFestDemoTest\bin\Debug\TestsToRun.txt");
            string[] testcases = lines.Split(',');
            
            foreach (string testcase in testcases)
            {
                Type myTypeObj = TC.GetType();
                MethodInfo myMethodInfo = myTypeObj.GetMethod(testcase);
                try
                {
                    myMethodInfo.Invoke(TC, null);
                }
                catch (Exception e)
                {
                    Console.WriteLine(testcase + " :: Test method does not exists.");
                    MessageLogger.Log(e);
                }

            }
        }
        public class TestCases
        {
            //protected ILog MessageLogger { get; set; }

            //public TestCases()
            //{
            //    MessageLogger = Injector.GetObject<ILog>();
            //}
            public void TC01()
            {
                Console.WriteLine("TC01 is called");
                //MessageLogger.Log("TC01 is called");
                string driverpath = @"C:\Users\rsantosh\Desktop\SelenimDriver";
                ChromeDriverService chromeService = ChromeDriverService.CreateDefaultService(driverpath, "chromedriver.exe");
                IWebDriver driver = new ChromeDriver(chromeService);
                driver.Navigate().GoToUrl("http://gmail.com");
            }

            public void TC02()
            {
                //MessageLogger.Log("TC02 is called");
            }

            public void TC03()
            {
                //MessageLogger.Log("TC03 is called");
            }
            public void TC04()
            {
                //MessageLogger.Log("TC04 is called");
            }
            public void TC05()
            {
                //MessageLogger.Log("TC05 is called");
            }
            public void TC06()
            {
                //MessageLogger.Log("TC06 is called");
            }
            public void TC07()
            {
                //MessageLogger.Log("TC07 is called");

            }
            public void TC08()
            {
                //MessageLogger.Log("TC08 is called");

            }
            public void TC09()
            {
                //MessageLogger.Log("TC09 is called");
            }
        }
    }
}
