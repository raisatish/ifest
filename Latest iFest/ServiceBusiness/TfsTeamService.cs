﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Models;
using Interfaces;
using System.Collections;
using Newtonsoft.Json;
using System.Xml.Linq;
using System.IO;
using System.Xml.Serialization;
using System.Configuration;

namespace ServiceBusiness
{
    public class TfsTeamService : ISourceVersionControl
    {

        private string TfsBaseAddr = System.Configuration.ConfigurationManager.AppSettings["tfsURL"].ToString();
        private static string TfsProjName;// = System.Configuration.ConfigurationManager.AppSettings["tfsProjectName"].ToString();
        private string TfsChangesetList;
        private const string TfsChangesetDetails = @"_apis/tfvc/changesets/";
        private const string TfsApiVersion = "api-version=1.0";
        private string AccessToken = System.Configuration.ConfigurationManager.AppSettings["tfsToken"].ToString();
        private int latestChangesetNo = 0;
        public string commitPreviousID;
        string commitIDs = "";
         
        //private string _configFile = ConfigurationManager.AppSettings.Get("C:\\Users\\Santhosh\\Desktop\\iFest\\IfestTool\\SchedulerConfig.xml");
        private List<TfsModifiedClass> lstModifiedClassItems { get; set; }

        private IRepository<SchedulerSettings> settingsRepository { get; set; }
        SchedulerSettings config = null;

        private ILog logger { get; set; }

        public TfsTeamService(ILog log)
        {
            GetCommitID();
            TfsProjName = config.ProjectName;
            TfsChangesetList = $"{TfsProjName}/_apis/git/repositories/";
            logger = log;
        }

        public void ServerUrl(string serverUrl)
        {
            TfsBaseAddr = serverUrl;
        }

        public void ProjectName(string projectName)
        {
            TfsChangesetList = $"{projectName}/_apis/tfvc/changesets/";
        }

        public void SetAccessToken(string accessToken)
        {
            AccessToken = accessToken;
        }

        public string[] GetModifiedClassesList(int fromChangesetNumber)
        {
            return GetModifiedClassesList(fromChangesetNumber.ToString());
        }

        public string[] GetModifiedClassesList(string fromChangesetNumber)
        {
            try
            {
                logger.Log($"Retrieving modified classes from changeset {fromChangesetNumber}");
                lstModifiedClassItems = new List<TfsModifiedClass>();
                int[] lstChangesetNumbers = { };
                GetCommitID();
                commitPreviousID = config.gitPriviousCommitID;
                using (var httpClient = new System.Net.Http.HttpClient())
                {
                    httpClient.BaseAddress = new Uri(TfsBaseAddr);
                    httpClient.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                    httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",
                            Convert.ToBase64String(Encoding.ASCII.GetBytes($":{AccessToken}")));

                    #region Retrieving changesets list associated with project after input changeset 
                    var response = httpClient.GetAsync("_apis/git/repositories/").Result;
                    IList repoId = new List<int>();
                    List<String> paths = new List<string>();
                 
                    if (response.IsSuccessStatusCode)
                    {
                        var respMsg = response.Content.ReadAsStringAsync().Result;
                        var tfsResp = Newtonsoft.Json.JsonConvert.DeserializeObject<TfsGitChangeSetListResponse>(respMsg);
                        var repoName = tfsResp.value.Select(c => c.name).ToArray();
                        commitIDs = string.Empty;
                        for (int i = 0; i < repoName.Count(); i++)
                        {
                            if (repoName.GetValue(i).ToString().ToLower().Equals(TfsProjName.ToLower()))
                            {
                                repoId = tfsResp.value.Select(c => c.id).ToList();
                                response = httpClient.GetAsync("_apis/git/repositories/" + repoId[i] + "/commits?api-version=1.0").Result;
                                respMsg = response.Content.ReadAsStringAsync().Result;

                                dynamic commits = Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(respMsg);
                                int commitIdsCount = commits.count;
                                DateTime? preDate = null;
                                for (int commitIndex = commitIdsCount - 1; commitIndex >= 0; commitIndex--)
                                {
                                    commitIDs = commits.value[commitIndex].commitId;
                                    DateTime date = commits.value[commitIndex].author.date;
                                    if (commitIDs.Equals(commitPreviousID))
                                    {
                                        preDate = commits.value[commitIndex].author.date;
                                    }
                                    if (date >= preDate)
                                    {
                                        var commitResponse = httpClient.GetAsync("_apis/git/repositories/" + repoId[i] + "/diffs/commits?baseVersionType=commit&baseVersion=" + commitPreviousID + "&targetVersionType=commit&targetVersion=" + commitIDs + "&api-version=1.0").Result;
                                        var commitRespMsg = commitResponse.Content.ReadAsStringAsync().Result;

                                        dynamic json = JsonConvert.DeserializeObject<dynamic>(commitRespMsg);
                                        int count = json.changes.Count;
                                        for (int deltaCount = 0; deltaCount < count; deltaCount++)
                                        {
                                            if (json.changes[deltaCount].item.path.ToString().Contains(".cs"))
                                            {
                                                paths.Add(json.changes[deltaCount].item.path.ToString());
                                            }
                                        }
                                    }
                                    if (commitPreviousID=="")
                                    {
                                        var commitResponse = httpClient.GetAsync("_apis/git/repositories/" + repoId[i] + "/diffs/commits?baseVersionType=commit&baseVersion=" + commitIDs + "&targetVersionType=commit&targetVersion=" + commitIDs + "&api-version=1.0").Result;
                                        var commitRespMsg = commitResponse.Content.ReadAsStringAsync().Result;

                                        dynamic json = JsonConvert.DeserializeObject<dynamic>(commitRespMsg);
                                        int count = json.changes.Count;
                                        for (int deltaCount = 0; deltaCount < count; deltaCount++)
                                        {
                                            if (json.changes[deltaCount].item.path.ToString().Contains(".cs"))
                                            {
                                                paths.Add(json.changes[deltaCount].item.path.ToString());
                                            }
                                        }
                                        commitPreviousID = commits.value[commitIndex].commitId;
                                        preDate = commits.value[commitIndex].author.date;
                                    }
                                }
                                // SchedulerSettings.ChangesetNo = commitPreviousID;
                                break;
                            }
                        }
                    }
                    foreach (string pathName in paths)
                    {
                        lstModifiedClassItems.Add(new TfsModifiedClass { TfsClassPath = pathName });

                    }
                    #endregion

                    #region Preparing distinct list of modified class files
                    var modifiedClassesOnly = lstModifiedClassItems
                        .Where(c => c.TfsClassPath.EndsWith(".cs"))
                        .Select(c => c.TfsClassPath).Distinct<string>();
                    #endregion
                  
                    return modifiedClassesOnly.ToArray();

                }
            }
            catch (Exception ex)
            {
                logger.Log(ex);
            }

            return new[] { "" };
        }

        public void SetUserName(string userName)
        {
            throw new NotImplementedException();
        }

        public void SetPassword(string password)
        {
            throw new NotImplementedException();
        }

        public int GetLatestProcessedChangesetNo()
        {
            return latestChangesetNo;
        }
        public string GetLatestProcessedCommitID()
        {
            commitPreviousID = commitIDs;
            return commitIDs;
        }



        public string GetCommitID()
        {
            XDocument xdocument = loadXML();
            
            IEnumerable<XElement> elems = xdocument.Elements();

            if (elems != null && elems.Count() > 0)
            {
               
                XElement elem = elems.First();
                config = new SchedulerSettings();
                config.Frequency = elem.Element("Frequency").Value;
                config.StartTime = elem.Element("StartTime").Value;

                if (!string.IsNullOrEmpty(elem.Element("Interval").Value))
                {
                    config.Interval = Convert.ToInt32(elem.Element("Interval").Value);
                }
                else
                {
                    config.Interval = 0;
                }

                config.SourceControlType = elem.Element("SourceControlType").Value;
             //   config.ChangesetNo = elem.Element("ChangesetNo").Value;
                config.ServerUrl = elem.Element("ServerUrl").Value;
                config.gitPriviousCommitID = elem.Element("ChangesetNo").Value.ToString();
                config.ServerUserName = elem.Element("ServerUserName").Value;
                config.ServerPassword = elem.Element("ServerPassword").Value;
                config.ServerAccessToken = elem.Element("ServerAccessToken").Value;
                config.ProjectName = elem.Element("ProjectName").Value;
            }
            return null;
        }
        private XDocument loadXML()
        {
            XDocument document = new XDocument();
            string strFolderPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            string path = strFolderPath+"\\IfestTool\\SchedulerConfig.xml";
            if (!File.Exists(path))
            {
                SchedulerSettings settings = new SchedulerSettings();
                XmlSerializer serializer = new XmlSerializer(typeof(SchedulerSettings));
                System.IO.Stream ms = File.OpenWrite(path);

                settings.Interval = 12;
                settings.StartTime = "00:00";
                settings.Frequency = "Hourly";

                settings.SourceControlType = "TFS";
                settings.ChangesetNo = string.Empty;
                settings.ServerUrl = string.Empty;
                settings.ServerUserName = string.Empty;
                settings.ServerPassword = string.Empty;
                settings.ServerAccessToken = string.Empty;
                settings.gitPriviousCommitID = string.Empty;
                settings.ProjectName = string.Empty;

                serializer.Serialize(ms, settings);
                

                ms.Flush();
                ms.Close();
                ms.Dispose();
                serializer = null;
            }

            document = XDocument.Load(path);

            return document;
        }

    }

    public static class UrlExtensions
    {
        public static string WithQuestionMark(this string urlComponent)
        {
            return "?" + urlComponent;
        }

        public static string WithAmpersand(this string urlComponent)
        {
            return "&" + urlComponent;
        }
    }



    

}



