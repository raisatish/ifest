﻿using Interfaces;
using Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace ServiceBusiness
{
    public class CommServer : ICommServer
    {
        //Event to pass recived data to the main class
        public event ServerDataEventHandler GotDataFromClient;

        private bool ServerRunning { get; set; }

        private IPAddress ipAddress { get; set; }
        private int PortNumber { get; set; }

        private ILog logger;

        private NetworkStream ClientStream { get; set; }

        public CommServer()
        {
            ClientStream = null;
        }

        public void SetLogger(ILog log)
        {
            logger = log;
        }

        public bool Start()
        {
            ipAddress = IPAddress.Parse("127.0.0.8");
            PortNumber = 369;

            ServerRunning = true;

            new Thread(() =>
            {
                try
                {
                    // TcpListener server = new TcpListener(port);
                    TcpListener server = new TcpListener(ipAddress, PortNumber);

                    // Start listening for client requests.

                    server.Stop();
                    Thread.Sleep(1000);
                    server.Start();

                    logger.Log("Waiting for a connection... ");

                    // Perform a blocking call to accept requests.
                    TcpClient client = server.AcceptTcpClient();
                    ClientStream = client.GetStream();
                    logger.Log("Client Connected!");

                    SendCommand(Commands.ServerSettings);

                    // Enter the listening loop.
                    while (ServerRunning)
                    {
                        // Buffer for reading data
                        var bytes = new byte[1024];
                        string data = null;

                        int i;

                        // Loop to receive all the data sent by the client.
                        while ((i = ClientStream.Read(bytes, 0, bytes.Length)) != 0)
                        {
                            // Translate data bytes to a ASCII string.
                            data = Encoding.ASCII.GetString(bytes, 0, i);
                            GotDataFromClient(client, data);
                        }
                    }

                    // Shutdown and end connection
                    client.Close();
                    server.Stop();
                }
                catch (SocketException e)
                {
                    logger.Log(e);
                }
                catch(Exception ex)
                {
                    logger.Log(ex);
                }
            }).Start();

            return true;
        }

        public void SendCommand(string Command)
        {
            try
            {
                var cmdText = JsonConvert.SerializeObject(new { Command = Command });

                byte[] msg = Encoding.ASCII.GetBytes(cmdText);

                ClientStream.Write(msg, 0, msg.Length);
            }
            catch (Exception ex)
            {
                logger.Log(ex);
            }
        }

        public bool Stop()
        {
            ServerRunning = false;

            ClientStream.Close();

            return true;
        }

        public void SendCommandWithData(string Command, string CommandData)
        {
            try
            {
                var cmdText = JsonConvert.SerializeObject(new { Command = Command, CommandData = CommandData });

                byte[] msg = Encoding.ASCII.GetBytes(cmdText);

                ClientStream.Write(msg, 0, msg.Length);
            }
            catch (Exception ex)
            {
                logger.Log(ex);
            }
        }
    }
}
