﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;

namespace ServiceBusiness
{
    public class CommClient : ICommClient
    {
        public event ClientDataEventHandler GotDataFromServer;

        private NetworkStream ServerStream { get; set; }

        private const int chunkSize = 1024;

        //Defines if the client shuld look for incoming data
        private bool StayConnected = false;

        public CommClient()
        {
        }

        public string GetId()
        {
            return "";
        }

        public bool Connect(string ip, int port)
        {
            try
            {
                StayConnected = true;

                new Thread(() =>
                {
                    try
                    {
                        TcpClient client = new TcpClient();
                        client.Connect(ip, port);

                        ServerStream = client.GetStream();

                        while (StayConnected)
                        {
                            // String to store the response ASCII representation.
                            var sbResponse = new StringBuilder();
                            int readBytes = 0;
                            do
                            {
                                var data = new byte[chunkSize];

                                // Read the first batch of the TcpServer response bytes.
                                readBytes = ServerStream.Read(data, 0, chunkSize);
                                sbResponse.Append(Encoding.ASCII.GetString(data, 0, readBytes));
                            }
                            while (readBytes >= chunkSize);


                            GotDataFromServer(client, sbResponse.ToString());

                            Thread.Sleep(200);
                        }

                        // Close everything.
                        ServerStream.Close();
                        client.Close();
                    }
                    catch (ArgumentNullException e)
                    {
                        Console.WriteLine("ArgumentNullException: {0}", e);
                    }
                    catch (SocketException e)
                    {
                        Console.WriteLine("SocketException: {0}", e);
                    }
                }).Start();

                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool Send(string CommandData)
        {
            try
            {
                // Translate the passed message into ASCII and store it as a Byte array.
                byte[] data = Encoding.ASCII.GetBytes(CommandData);

                // Get a client stream for reading and writing.
                //  Stream stream = client.GetStream();

                // Send the message to the connected TcpServer. 
                ServerStream.Write(data, 0, data.Length);

                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
            return false;
        }

        //Closes the connection
        public void CloseClient()
        {
            //Stop listening
            StayConnected = false;

            //Close stream
            ServerStream.Close();
        }
    }
}
