﻿namespace Interfaces
{
    public interface ISourceVersionControl
    {
        void ServerUrl(string serverUrl);
        void ProjectName(string projectName);
        void SetAccessToken(string accessToken);
        void SetUserName(string userName);
        void SetPassword(string password);
        string[] GetModifiedClassesList(int fromChangesetNumber);
        string[] GetModifiedClassesList(string fromChangesetNumber);
        int GetLatestProcessedChangesetNo();
        string GetLatestProcessedCommitID();
    }
}
