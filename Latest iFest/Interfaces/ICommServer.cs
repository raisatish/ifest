﻿namespace Interfaces
{
    public delegate void ServerDataEventHandler(object sender, string msg);

    public interface ICommServer
    {
        bool Start();
        void SendCommand(string Command);
        void SendCommandWithData(string Command, string CommandData);
        bool Stop();
        void SetLogger(ILog log);
        event ServerDataEventHandler GotDataFromClient;
    }
}
