﻿using System.Net.Sockets;

namespace Interfaces
{
    //Event to pass recived data to the server class
    public delegate void ClientDataEventHandler(object sender, string msg);

    public interface ICommClient
    {
        string GetId();
        bool Connect(string ip, int port);
        bool Send(string CommandData);
        event ClientDataEventHandler GotDataFromServer;
    }
}
