﻿using System;
using Models;

namespace Interfaces
{
    public interface ILog
    {
        void Log(string Message);
        void Log(string Message, LogType LT);
        void Log(Exception ex);
    }
}
