﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IRepository<T>
    {
        List<T> All();
        T Get(string id);
        T Save(T o);
        bool Update(string id, T newObject);
        bool UpdateScheduler(string id, T newObject);
        bool Delete(string id);
    }
}
