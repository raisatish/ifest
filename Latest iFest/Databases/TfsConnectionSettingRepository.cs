﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.IO;
using System.Xml.Serialization;
using System.Configuration;
using System.Globalization;
using Models;
using Interfaces;
using ServiceBusiness;

namespace Databases
{
    public class TfsConnectionSettingRepository : IRepository<TfsConnectionDetails>
    {
        private string _configFile = ConfigurationManager.AppSettings.Get("TfsConncetionFile");


        public List<TfsConnectionDetails> All()
        {
            throw new NotImplementedException();
        }

        public bool Delete(string id)
        {
            throw new NotImplementedException();
        }

        public TfsConnectionDetails Get(string id)
        {
            TfsConnectionDetails config = null;
            XDocument xdocument = loadXML();
            IEnumerable<XElement> elems = xdocument.Elements();

            if (elems != null && elems.Count() > 0)
            {
                XElement elem = elems.First();

                config = new TfsConnectionDetails();
                
                config.ServerUrl = elem.Element("ServerUrl").Value;                
                config.ServerUserName = elem.Element("ServerUserName").Value;
                config.ServerPassword = elem.Element("ServerPassword").Value;
                config.ProjectName = elem.Element("ProjectName").Value;
                config.TestServerType = elem.Element("TestServerType").Value;
                config.TestPlanId = Convert.ToInt32(elem.Element("TestPlanId").Value);
                config.TestSuiteId = Convert.ToInt32(elem.Element("TestSuiteId").Value);
                config.TestSettingId = Convert.ToInt32(elem.Element("TestSettingId").Value);
                config.ConfigurationId = Convert.ToInt32(elem.Element("ConfigurationId").Value);
                config.EnvironmentName = elem.Element("EnvironmentName").Value;                
            }

            return config;
        }

        public TfsConnectionDetails Save(TfsConnectionDetails o)
        {
            throw new NotImplementedException();
        }

        public bool Update(string id, TfsConnectionDetails config)
        {
            XDocument xdocument = loadXML();

            IEnumerable<XElement> elems = xdocument.Elements();

            if (elems != null && elems.Count() > 0)
            {
                XElement elem = elems.First();                               
                elem.Element("ServerUrl").Value = config.ServerUrl;
                elem.Element("ServerUserName").Value = config.ServerUserName;
                elem.Element("ServerPassword").Value = config.ServerPassword;
                //elem.Element("TestServerType").Value = config.TestServerType;
                //elem.Element("ProjectName").Value = config.ProjectName;
                elem.Element("TestPlanId").Value = config.TestPlanId.ToString();
                elem.Element("TestSuiteId").Value = config.TestSuiteId.ToString();
                elem.Element("TestSettingId").Value = config.TestSettingId.ToString();
                elem.Element("ConfigurationId").Value = config.ConfigurationId.ToString();
                elem.Element("EnvironmentName").Value = config.EnvironmentName;

                xdocument.Save(_configFile);
                xdocument = null;
                return true;
            }
            return false;
        }

        public bool UpdateScheduler(string id, TfsConnectionDetails config)
        {
            throw new NotImplementedException();
        }

        private XDocument loadXML()
        {
            XDocument document = new XDocument();

            if (!File.Exists(_configFile))
            {
                TfsConnectionDetails settings = new TfsConnectionDetails();
                XmlSerializer serializer = new XmlSerializer(typeof(TfsConnectionDetails));
                System.IO.Stream ms = File.OpenWrite(_configFile);                
                settings.ServerUrl = string.Empty;
                settings.ServerUserName = string.Empty;
                settings.ServerPassword = string.Empty;
                settings.ProjectName = string.Empty;
                settings.TestPlanId = 0;                
                serializer.Serialize(ms, settings);

                ms.Flush();
                ms.Close();
                ms.Dispose();
                serializer = null;
            }

            document = XDocument.Load(_configFile);

            return document;
        }
    }
}
