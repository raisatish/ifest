﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;

using Models;
using Interfaces;

namespace Databases
{
    public class TestRepository : IRepository<STest>
    {
        private string _configFile = ConfigurationManager.AppSettings.Get("TestConfigFile");

        public List<STest> All()
        {
            XDocument doc = loadXML();

            List<STest> lstTests = (from t in doc.Descendants().Elements("Test")
                                    select new STest
                                    {
                                        Id = new Guid(t.Element("Id").Value),
                                        Name = t.Element("Name").Value
                                    }).ToList();

            return lstTests;
        }

        public STest Save(STest t)
        {
            throw new NotImplementedException();
        }

        public bool Delete(string id)
        {
            XDocument doc = loadXML();

             XElement tst = doc.Descendants("Test").FirstOrDefault(t => t.Element("Id").Value == id);
             if (tst != null)
             {
                 tst.Remove();
                 doc.Save(_configFile);

                 return true;
             }

             return false;
        }

        public STest Get(string id)
        {
            throw new NotImplementedException();
        }

        public bool Update(string id, STest newObject)
        {
            throw new NotImplementedException();
        }
        public bool UpdateScheduler(string id, STest newObject)
        {
            throw new NotImplementedException();
        }

        private XDocument loadXML()
        {
            XDocument document = new XDocument();

            document = XDocument.Load(_configFile);

            return document;
        }
    }
}