﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.IO;
using System.Xml.Serialization;
using System.Configuration;
using System.Globalization;

using Models;
using Interfaces;
using ServiceBusiness;

namespace Databases
{
    public class SettingsRepository : IRepository<SchedulerSettings>
    {
        private string _configFile = ConfigurationManager.AppSettings.Get("SchedulerConfigFile");
        

        public List<SchedulerSettings> All()
        {
            throw new NotImplementedException();
        }

        public bool Delete(string id)
        {
            throw new NotImplementedException();
        }

        public SchedulerSettings Get(string id)
        {
            SchedulerSettings config = null;
            XDocument xdocument = loadXML();
            IEnumerable<XElement> elems = xdocument.Elements();

            if (elems != null && elems.Count() > 0)
            {
                XElement elem = elems.First();

                config = new SchedulerSettings();
                config.Frequency = elem.Element("Frequency").Value;
                config.StartTime = elem.Element("StartTime").Value;

                if (!string.IsNullOrEmpty(elem.Element("Interval").Value))
                {
                    config.Interval = Convert.ToInt32(elem.Element("Interval").Value);
                }
                else
                {
                    config.Interval = 0;
                }

                config.SourceControlType = elem.Element("SourceControlType").Value;
                config.ChangesetNo = elem.Element("ChangesetNo").Value;
                config.ServerUrl = elem.Element("ServerUrl").Value;
                config.gitPriviousCommitID = elem.Element("ChangesetNo").Value.ToString();
                config.ServerUserName = elem.Element("ServerUserName").Value;
                config.ServerPassword = elem.Element("ServerPassword").Value;
                config.ServerAccessToken = elem.Element("ServerAccessToken").Value;
                config.ProjectName = elem.Element("ProjectName").Value;
                config.ColorCombination = elem.Element("ColorCombination").Value;
                config.FilestoExclude = elem.Element("FilestoExclude").Value;
            }

            return config;
        }

        public SchedulerSettings Save(SchedulerSettings o)
        {
            throw new NotImplementedException();
        }

        public bool Update(string id, SchedulerSettings config)
        {
            XDocument xdocument = loadXML();

            IEnumerable<XElement> elems = xdocument.Elements();

            if (elems != null && elems.Count() > 0)
            {
                XElement elem = elems.First();

                //elem.Element("Frequency").Value = config.Frequency;
                //elem.Element("StartTime").Value = config.StartTime;
                //elem.Element("Interval").Value = config.Interval.HasValue ? config.Interval.Value.ToString() : "";

                elem.Element("SourceControlType").Value = config.SourceControlType;
                elem.Element("ChangesetNo").Value = config.ChangesetNo;
                elem.Element("ServerUrl").Value = config.ServerUrl;
                elem.Element("ServerUserName").Value = config.ServerUserName;
                elem.Element("ServerPassword").Value = config.ServerPassword;
                elem.Element("ServerAccessToken").Value = config.ServerAccessToken;
                elem.Element("ProjectName").Value = config.ProjectName;
                xdocument.Save(_configFile);

                xdocument = null;
                return true;
            }
            return false;
        }

        public bool UpdateScheduler(string id, SchedulerSettings config)
        {
            XDocument xdocument = loadXML();

            IEnumerable<XElement> elems = xdocument.Elements();

            if (elems != null && elems.Count() > 0)
            {
                XElement elem = elems.First();

                elem.Element("Frequency").Value = config.Frequency;
                elem.Element("StartTime").Value = config.StartTime;
                elem.Element("Interval").Value = config.Interval.HasValue ? config.Interval.Value.ToString() : "";
                xdocument.Save(_configFile);

                xdocument = null;
                return true;
            }
            return false;
        }

        private XDocument loadXML()
        {
            XDocument document = new XDocument();

            if (!File.Exists(_configFile))
            {
                SchedulerSettings settings = new SchedulerSettings();
                XmlSerializer serializer = new XmlSerializer(typeof(SchedulerSettings));
                System.IO.Stream ms = File.OpenWrite(_configFile);

                settings.Interval = 12;
                settings.StartTime = "00:00";
                settings.Frequency = "Hourly";

                settings.SourceControlType = "TFS";
                settings.ChangesetNo = string.Empty;
                settings.ServerUrl = string.Empty;
                settings.ServerUserName = string.Empty;
                settings.ServerPassword = string.Empty;
                settings.ServerAccessToken = string.Empty;
                settings.ProjectName = string.Empty;
                serializer.Serialize(ms, settings);

                ms.Flush();
                ms.Close();
                ms.Dispose();
                serializer = null;
            }
           
            document = XDocument.Load(_configFile);
           
            return document;
        }
    }
}