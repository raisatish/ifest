﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.IO;
using System.Configuration;

using Models;
using Interfaces;

namespace Databases
{
    public class ClassRepository : IRepository<SClass>
    {
        private string _configFile = ConfigurationManager.AppSettings.Get("TestConfigFile");

        public List<SClass> All()
        {
            checkAndCreateConfig();

            XDocument doc = loadXML();
            
            List<SClass> lstClasses = (from c in doc.Descendants().Elements("Class")
                                       select new SClass
                                       {
                                           Name = c.Element("Name").Value,
                                           Id = new Guid(c.Element("Id").Value),
                                           Tests = new List<STest>(from t in c.Element("Tests").Elements("Test")
                                                    select new STest
                                                    {
                                                        Id = new Guid(t.Element("Id").Value),
                                                        Name = t.Element("Name").Value
                                                    }
                                           )
                                       }
                                      ).ToList();

            return lstClasses;
        }

        public SClass Save(SClass o)
        {
            checkAndCreateConfig();

            XDocument doc = loadXML();

            var clsExisting = from c in doc.Descendants().Elements("Class")
                                 where c.Element("Name").Value.ToLower() == o.Name.ToLower()
                                 select (c.Name);
            if(clsExisting.Count() == 0)
            {
                XElement oClass = new XElement("Class");
                oClass.Add(new XElement("Name", o.Name));
                oClass.Add(new XElement("Id", Guid.NewGuid()));
                oClass.Add(new XElement("Tests", ""));

                doc.Descendants().Elements("Classes").First().Add(oClass);

                doc.Save(_configFile);

                return o;
            }
            else
            {
                return null;
            }
        }

        public bool Delete(string id)
        {
            XDocument doc = loadXML();

            XElement cls = doc.Descendants("Class").FirstOrDefault(c => c.Element("Id").Value == id);
            if (cls != null)
            {
                cls.Remove();
                doc.Save(_configFile);

                return true;
            }

            return false;
        }

        public SClass Get(string id)
        {
            SClass cls = null;

            XDocument doc = loadXML();

           
            cls = (from c in doc.Descendants().Elements("Class")
                       where c.Element("Id").Value == id
                       select new SClass{
                           Name = c.Element("Name").Value,
                           Id = new Guid(c.Element("Id").Value),
                           Tests = new List<STest>(from t in c.Element("Tests").Elements("Test")
                                                   select new STest
                                                   {
                                                       Id = new Guid(t.Element("Id").Value),
                                                       Name = t.Element("Name").Value
                                                   }
                           )
                       }).ToList().First();


            return cls;
        }

        public bool Update(string id, SClass newObject)
        {
            XDocument doc = loadXML();

             XElement cls = doc.Descendants("Class").FirstOrDefault(c => c.Element("Id").Value == id);

             if (cls != null)
             {
                 XElement tests = cls.Element("Tests");

                 XElement test = new XElement("Test" , new XElement("Id", Guid.NewGuid()) ,
                                                       new XElement("Name", newObject.Tests[0].Name)
                                              );

                 tests.Add(test);
                 doc.Save(_configFile);

                 return true;
             }

             return false;
        }

        public bool UpdateScheduler(string id, SClass newObject)
        {
            throw new NotImplementedException();
        }
        private XDocument loadXML()
        {
            XDocument document = new XDocument();

            document = XDocument.Load(_configFile);

            return document;
        }

        private void checkAndCreateConfig()
        {
            if (!File.Exists(_configFile))
            {
                TestsConfig cnfg = new TestsConfig();

                cnfg.Classes = new List<SClass>();

                XmlSerializer serializer = new XmlSerializer(typeof(TestsConfig));
                System.IO.Stream ms = File.OpenWrite(_configFile);
                
                serializer.Serialize(ms, cnfg);

                ms.Flush();
                ms.Close();
                ms.Dispose();
                serializer = null;
            }
        }
    }
}