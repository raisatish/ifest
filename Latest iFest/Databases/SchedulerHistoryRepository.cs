﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Models;
using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml;

namespace Databases
{
    public class SchedulerHistoryRepository : IRepository<SchExecResult>
    {
        private string _resFile = ConfigurationManager.AppSettings.Get("SchedulerHistoryFile");


        public List<SchExecResult> All()
        {
            List<SchExecResult> results = null;

            if(File.Exists(_resFile))
            {
                XDocument doc = loadXML();

                results = (from r in doc.Descendants().Elements("Result")
                                        select new SchExecResult
                                        {
                                            ExecDate = Convert.ToDateTime(r.Element("ExecDate").Value),
                                            ModifiedClasses = new List<DiffClass>(from c in r.Element("ModifiedClasses").Elements("Class")
                                                                    select new DiffClass
                                                                    {
                                                                      ClassName = c.Attribute("Name").Value,
                                                                      Tests =  (c.Attribute("Tests") != null) ? c.Attribute("Tests").Value : string.Empty,
                                                                    }
                                           )
                                        }).OrderByDescending(x=>x.ExecDate)
                                        .ToList();
            }

            return results;
        }

        public bool Delete(string id)
        {
            throw new NotImplementedException();
        }

        public SchExecResult Get(string id)
        {
            throw new NotImplementedException();
        }

        public SchExecResult Save(SchExecResult res)
        {
            XDocument document;
            XElement mClass;

            if (!File.Exists(_resFile))
            {
                SchExecResults results = new SchExecResults();
                XmlSerializer serializer = new XmlSerializer(typeof(SchExecResults));
                System.IO.Stream ms = File.OpenWrite(_resFile);

                serializer.Serialize(ms, results);

                ms.Flush();
                ms.Close();
                ms.Dispose();
                serializer = null;
            }

            document = loadXML();

            XElement oResult = new XElement("Result");
            oResult.Add(new XElement("ExecDate", res.ExecDate));

            XElement oModClasses = new XElement("ModifiedClasses");

            if (res.ModifiedClasses != null)
                foreach (DiffClass cls in res.ModifiedClasses)
                {
                    mClass = new XElement("Class");

                    mClass.SetAttributeValue("Name", cls.ClassName);
                    mClass.SetAttributeValue("Tests", cls.Tests);

                    oModClasses.Add(mClass);
                }
            oResult.Add(oModClasses);

            document.Descendants("Results").LastOrDefault().Add(oResult);

            document.Save(_resFile);

            return res;
        }

        public bool Update(string id, SchExecResult res)
        {
            throw new NotImplementedException();
        }

        public bool UpdateScheduler(string id, SchExecResult res)
        {
            throw new NotImplementedException();
        }
        private XDocument loadXML()
        {
            XDocument document = new XDocument();

            document = XDocument.Load(_resFile);

            return document;
        }


    }
}
