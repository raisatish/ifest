﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Interfaces;

namespace Databases
{
    public abstract class Repository<T> : IRepository<T>
    {
        public List<T> All()
        {
            throw new NotImplementedException();
        }

        public bool Delete(string id)
        {
            throw new NotImplementedException();
        }

        public T Get(string id)
        {
            throw new NotImplementedException();
        }

        public T Save(T o)
        {
            throw new NotImplementedException();
        }

        public bool Update(string id, T newObject)
        {
            throw new NotImplementedException();
        }

        public bool UpdateScheduler(string id, T newObject)
        {
            throw new NotImplementedException();
        }
    }
}
