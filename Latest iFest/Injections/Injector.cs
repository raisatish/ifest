﻿using Databases;
using Interfaces;
using Models;
using ServiceBusiness;
using System;

namespace Injections
{
    public static class Injector
    {
        private static readonly ILog logger = new Logger.LogToFile();
        private static IRepository<SchedulerSettings> settingsRepository;
        private static IRepository<SClass> clsRepository;
        private static IRepository<STest> tstRepository;
        private static ICommServer commServer;
        private static IRepository<SchExecResult> schExecResRepository;
        private static IRepository<TfsConnectionDetails> tfsSettingsRepository;
        private static IRepository<AzureDevOpsConnectionDetails> azureSettingsRepository;

        private static object logLock = new object();

        public static T GetObject<T>() where T: class
        {
            try
            {
                if (typeof(T).FullName.Equals(typeof(ILog).FullName, StringComparison.InvariantCulture))
                {
                    return (T)logger;
                }

                if (typeof(T).FullName.Equals(typeof(IRepository<SchedulerSettings>).FullName, StringComparison.InvariantCulture))
                {
                    if (settingsRepository == null)
                        settingsRepository = new SettingsRepository();

                    return (T)settingsRepository;
                }

                if (typeof(T).FullName.Equals(typeof(IRepository<TfsConnectionDetails>).FullName, StringComparison.InvariantCulture))
                {
                    if (tfsSettingsRepository == null)
                        tfsSettingsRepository = new TfsConnectionSettingRepository();

                    return (T)tfsSettingsRepository;
                }

                if (typeof(T).FullName.Equals(typeof(IRepository<AzureDevOpsConnectionDetails>).FullName, StringComparison.InvariantCulture))
                {
                    if (azureSettingsRepository == null)
                        azureSettingsRepository = new AzureDevOpsConnectionSettingRepository();

                    return (T)azureSettingsRepository;
                }

                if (typeof(T).FullName.Equals(typeof(IRepository<SClass>).FullName, StringComparison.InvariantCulture))
                {
                    if (clsRepository == null)
                        clsRepository = new ClassRepository();

                    return (T)clsRepository;
                }

                if (typeof(T).FullName.Equals(typeof(IRepository<STest>).FullName, StringComparison.InvariantCulture))
                {
                    if (tstRepository == null)
                        tstRepository = new TestRepository();

                    return (T)tstRepository;
                }

                if (typeof(T).FullName.Equals(typeof(ISourceVersionControl).FullName, StringComparison.InvariantCulture))
                {
                    return (T)(new TfsTeamService(GetObject<ILog>()) as ISourceVersionControl);
                }

                if (typeof(T).FullName.Equals(typeof(ICommServer).FullName, StringComparison.InvariantCulture))
                {
                    var server = new CommServer();
                    server.SetLogger(GetObject<ILog>());
                    return (T)(server as ICommServer);
                }

                if (typeof(T).FullName.Equals(typeof(ICommClient).FullName, StringComparison.InvariantCulture))
                {
                    var client = new CommClient();
                    return (T)(client as ICommClient);
                }

                if (typeof(T).FullName.Equals(typeof(IRepository<SchExecResult>).FullName, StringComparison.InvariantCulture))
                {
                    if (schExecResRepository == null)
                        schExecResRepository = new SchedulerHistoryRepository();

                    return (T)schExecResRepository;
                }

                return null;
            }
            catch (Exception ex)
            {
                logger.Log(ex);
                return null;
            }
        }
    }
}
